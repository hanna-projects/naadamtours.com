<?php

/**
 * Item form.
 *
 * @package    uaral
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ItemForm extends BaseItemForm
{
  public function configure()
  {
	  	$host = sfConfig::get('app_host');
      
      // WIDGETS
	  	$choices = GlobalTable::doFetchSelection('Page', 'title', 'title');
      $this->widgetSchema['page_id']     = new sfWidgetFormChoice(array('choices' => $choices), array('onchange'=>'toggleSuggestedOffers();'));
      $this->widgetSchema['title']       = new sfWidgetFormInputText(array(), array());
      $this->widgetSchema['title_de']    = new sfWidgetFormInputText(array(), array());
      $this->widgetSchema['title_it']    = new sfWidgetFormInputText(array(), array());
      $this->widgetSchema['title_ko']    = new sfWidgetFormInputText(array(), array());
	  	$this->widgetSchema['summary']     = new sfWidgetFormTextarea(array(), array());
      $this->widgetSchema['summary_de']  = new sfWidgetFormTextarea(array(), array());
      $this->widgetSchema['summary_it']  = new sfWidgetFormTextarea(array(), array());
      $this->widgetSchema['summary_ko']  = new sfWidgetFormTextarea(array(), array());
	  	$this->widgetSchema['image1']      = new sfWidgetFormInputFile(array(), array());
      $this->widgetSchema['content']     = new sfWidgetFormTextarea(array(), array());
      $this->widgetSchema['content_de']  = new sfWidgetFormTextarea(array(), array());
      $this->widgetSchema['content_it']  = new sfWidgetFormTextarea(array(), array());
      $this->widgetSchema['content_ko']  = new sfWidgetFormTextarea(array(), array());

      // VALIDATORS
      $this->validatorSchema['page_id']    = new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Page')));
      $this->validatorSchema['item_ids']   = new sfValidatorPass();
      $this->validatorSchema['title_de']   = new sfValidatorPass();
      $this->validatorSchema['title_it']   = new sfValidatorPass();
      $this->validatorSchema['title_ko']   = new sfValidatorPass();
      $this->validatorSchema['summary']	   = new sfValidatorPass();
      $this->validatorSchema['summary_de'] = new sfValidatorPass();
      $this->validatorSchema['summary_it'] = new sfValidatorPass();
      $this->validatorSchema['summary_ko'] = new sfValidatorPass();
	  	$this->validatorSchema['image1']     = new sfValidatorFile($this->getFileAttrs('item'), $this->getFileOpts());
	  	$this->validatorSchema['content']	   = new sfValidatorPass();
      $this->validatorSchema['content_de'] = new sfValidatorPass();
      $this->validatorSchema['content_it'] = new sfValidatorPass();
      $this->validatorSchema['content_ko'] = new sfValidatorPass();
  }
}

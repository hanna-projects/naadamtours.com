<?php

/**
 * Offer form.
 *
 * @package    uaral
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class OfferForm extends BaseOfferForm
{
  public function configure()
  {
  		$host = sfConfig::get('app_host');
      
      // WIDGETS		
      $this->widgetSchema['title']       						= new sfWidgetFormInputText(array(), array());
      $this->widgetSchema['title_de']    						= new sfWidgetFormInputText(array(), array());
      $this->widgetSchema['title_it']    						= new sfWidgetFormInputText(array(), array());
      $this->widgetSchema['title_ko']   			 			= new sfWidgetFormInputText(array(), array());
	  	$this->widgetSchema['intro']     					 		= new sfWidgetFormTextarea(array(), array());
      $this->widgetSchema['intro_de'] 	 						= new sfWidgetFormTextarea(array(), array());
      $this->widgetSchema['intro_it']  	 						= new sfWidgetFormTextarea(array(), array());
      $this->widgetSchema['intro_ko']  	 						= new sfWidgetFormTextarea(array(), array());	  	
      $this->widgetSchema['overview']    						= new sfWidgetFormTextarea(array(), array());
      $this->widgetSchema['overview_de'] 						= new sfWidgetFormTextarea(array(), array());
      $this->widgetSchema['overview_it'] 						= new sfWidgetFormTextarea(array(), array());
      $this->widgetSchema['overview_ko'] 						= new sfWidgetFormTextarea(array(), array());
      $this->widgetSchema['highlights']    					= new sfWidgetFormTextarea(array(), array());
      $this->widgetSchema['highlights_de'] 					= new sfWidgetFormTextarea(array(), array());
      $this->widgetSchema['highlights_it'] 					= new sfWidgetFormTextarea(array(), array());
      $this->widgetSchema['highlights_ko'] 					= new sfWidgetFormTextarea(array(), array());
      
      $this->widgetSchema['meal']    						 		= new sfWidgetFormInputText(array(), array());
      $this->widgetSchema['transport']  				 		= new sfWidgetFormInputText(array(), array());
      $this->widgetSchema['accommodation']    	 		= new sfWidgetFormInputText(array(), array());
      $this->widgetSchema['included_activities'] 		= new sfWidgetFormTextarea(array(), array());
      $this->widgetSchema['important_notes'] 		 		= new sfWidgetFormTextarea(array(), array());
      $this->widgetSchema['thumb']       			   		= new sfWidgetFormInputFile(array(), array());
      $this->widgetSchema['cover']      				 		= new sfWidgetFormInputFile(array(), array());
      $this->widgetSchema['map']        				 		= new sfWidgetFormInputFile(array(), array());
      $choices = GlobalLib::getNumbers(1, 100);
      $this->widgetSchema['nb_days']    				 		= new sfWidgetFormChoice(array('choices'=>$choices), array('style'=>'width:50px;'));
      $this->widgetSchema['guests']   					 		= new sfWidgetFormInputText(array(), array());
			$this->widgetSchema['price_from']  				 		= new sfWidgetFormInputText(array(), array());
			$this->widgetSchema['latitude']  				 	 		= new sfWidgetFormInputText(array(), array());
			$this->widgetSchema['longitude']  					  = new sfWidgetFormInputText(array(), array());

      // VALIDATORS
      $this->validatorSchema['title']   	  				= new sfValidatorString();
      $this->validatorSchema['title_de']    				= new sfValidatorString();
      $this->validatorSchema['title_it']    				= new sfValidatorString();
      $this->validatorSchema['title_ko']    				= new sfValidatorString();
      $this->validatorSchema['intro']	      				= new sfValidatorPass();
      $this->validatorSchema['intro_de']    				= new sfValidatorPass();
      $this->validatorSchema['intro_it']    				= new sfValidatorPass();
      $this->validatorSchema['intro_ko']    				= new sfValidatorPass();
	  	$this->validatorSchema['overview']	  				= new sfValidatorPass();
      $this->validatorSchema['overview_de'] 				= new sfValidatorPass();
      $this->validatorSchema['overview_it'] 				= new sfValidatorPass();
      $this->validatorSchema['overview_ko'] 				= new sfValidatorPass();
      $this->validatorSchema['highlights']	  			= new sfValidatorPass();
      $this->validatorSchema['highlights_de'] 			= new sfValidatorPass();
      $this->validatorSchema['highlights_it'] 			= new sfValidatorPass();
      $this->validatorSchema['highlights_ko'] 		 	= new sfValidatorPass();
      
      $this->validatorSchema['meal'] 								= new sfValidatorPass();
      $this->validatorSchema['transport'] 					= new sfValidatorPass();
      $this->validatorSchema['accommodation'] 		  = new sfValidatorPass();
      $this->validatorSchema['included_activities'] = new sfValidatorPass();
      $this->validatorSchema['important_notes'] 		= new sfValidatorPass();
      $this->validatorSchema['thumb']      				  = new sfValidatorFile($this->getFileAttrs('offer'), $this->getFileOpts());
      $this->validatorSchema['cover']      				  = new sfValidatorFile($this->getFileAttrs('offer'), $this->getFileOpts());
      $this->validatorSchema['map']     	 					= new sfValidatorFile($this->getFileAttrs('offer'), $this->getFileOpts());
      $this->validatorSchema['nb_days'] 					  = new sfValidatorPass();
      $this->validatorSchema['guests'] 						  = new sfValidatorPass();
      $this->validatorSchema['price_from'] 	        = new sfValidatorPass();
      $this->validatorSchema['latitude'] 					  = new sfValidatorPass();
      $this->validatorSchema['longitude'] 		      = new sfValidatorPass();
      
      
  }

}

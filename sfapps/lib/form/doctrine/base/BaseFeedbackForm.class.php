<?php

/**
 * Feedback form base class.
 *
 * @method Feedback getObject() Returns the current form's model object
 *
 * @package    uaral
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseFeedbackForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                        => new sfWidgetFormInputHidden(),
      'title'                     => new sfWidgetFormInputText(),
      'firstname'                 => new sfWidgetFormInputText(),
      'lastname'                  => new sfWidgetFormInputText(),
      'email'                     => new sfWidgetFormInputText(),
      'phone'                     => new sfWidgetFormInputText(),
      'besttime2call'             => new sfWidgetFormInputText(),
      'enquiry'                   => new sfWidgetFormTextarea(),
      'motivation'                => new sfWidgetFormTextarea(),
      'motivation_other'          => new sfWidgetFormTextarea(),
      'accommodation'             => new sfWidgetFormTextarea(),
      'accommodation_other'       => new sfWidgetFormTextarea(),
      'specialist_interest'       => new sfWidgetFormTextarea(),
      'specialist_interest_other' => new sfWidgetFormTextarea(),
      'guiding'                   => new sfWidgetFormTextarea(),
      'guiding_other'             => new sfWidgetFormTextarea(),
      'hear_source'               => new sfWidgetFormInputText(),
      'hear_source_other'         => new sfWidgetFormTextarea(),
      'country'                   => new sfWidgetFormInputText(),
      'state'                     => new sfWidgetFormInputText(),
      'city'                      => new sfWidgetFormInputText(),
      'ip_address'                => new sfWidgetFormInputText(),
      'created_at'                => new sfWidgetFormDateTime(),
      'seen_at'                   => new sfWidgetFormDateTime(),
      'response'                  => new sfWidgetFormTextarea(),
      'last_replied_at'           => new sfWidgetFormDateTime(),
      'created_aid'               => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Admin'), 'add_empty' => false)),
      'updated_aid'               => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Admin_2'), 'add_empty' => false)),
    ));

    $this->setValidators(array(
      'id'                        => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'title'                     => new sfValidatorString(array('max_length' => 255)),
      'firstname'                 => new sfValidatorString(array('max_length' => 255)),
      'lastname'                  => new sfValidatorString(array('max_length' => 255)),
      'email'                     => new sfValidatorString(array('max_length' => 255)),
      'phone'                     => new sfValidatorString(array('max_length' => 255)),
      'besttime2call'             => new sfValidatorString(array('max_length' => 255)),
      'enquiry'                   => new sfValidatorString(),
      'motivation'                => new sfValidatorString(),
      'motivation_other'          => new sfValidatorString(),
      'accommodation'             => new sfValidatorString(),
      'accommodation_other'       => new sfValidatorString(),
      'specialist_interest'       => new sfValidatorString(),
      'specialist_interest_other' => new sfValidatorString(),
      'guiding'                   => new sfValidatorString(),
      'guiding_other'             => new sfValidatorString(),
      'hear_source'               => new sfValidatorString(array('max_length' => 255)),
      'hear_source_other'         => new sfValidatorString(),
      'country'                   => new sfValidatorString(array('max_length' => 255)),
      'state'                     => new sfValidatorString(array('max_length' => 255)),
      'city'                      => new sfValidatorString(array('max_length' => 255)),
      'ip_address'                => new sfValidatorString(array('max_length' => 50)),
      'created_at'                => new sfValidatorDateTime(),
      'seen_at'                   => new sfValidatorDateTime(array('required' => false)),
      'response'                  => new sfValidatorString(),
      'last_replied_at'           => new sfValidatorDateTime(array('required' => false)),
      'created_aid'               => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Admin'))),
      'updated_aid'               => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Admin_2'))),
    ));

    $this->widgetSchema->setNameFormat('feedback[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Feedback';
  }

}

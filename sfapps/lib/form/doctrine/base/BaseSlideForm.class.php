<?php

/**
 * Slide form base class.
 *
 * @method Slide getObject() Returns the current form's model object
 *
 * @package    uaral
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseSlideForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'title'          => new sfWidgetFormInputText(),
      'title_de'       => new sfWidgetFormInputText(),
      'title_it'       => new sfWidgetFormInputText(),
      'title_ko'       => new sfWidgetFormInputText(),
      'filename'       => new sfWidgetFormInputText(),
      'link'           => new sfWidgetFormInputText(),
      'description'    => new sfWidgetFormTextarea(),
      'description_de' => new sfWidgetFormTextarea(),
      'description_it' => new sfWidgetFormTextarea(),
      'description_ko' => new sfWidgetFormTextarea(),
      'is_active'      => new sfWidgetFormInputText(),
      'is_featured'    => new sfWidgetFormInputText(),
      'sort'           => new sfWidgetFormInputText(),
      'created_at'     => new sfWidgetFormDateTime(),
      'updated_at'     => new sfWidgetFormDateTime(),
      'created_aid'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Admin'), 'add_empty' => false)),
      'updated_aid'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Admin_2'), 'add_empty' => false)),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'title'          => new sfValidatorString(array('max_length' => 255)),
      'title_de'       => new sfValidatorString(array('max_length' => 255)),
      'title_it'       => new sfValidatorString(array('max_length' => 255)),
      'title_ko'       => new sfValidatorString(array('max_length' => 255)),
      'filename'       => new sfValidatorString(array('max_length' => 255)),
      'link'           => new sfValidatorString(array('max_length' => 255)),
      'description'    => new sfValidatorString(),
      'description_de' => new sfValidatorString(),
      'description_it' => new sfValidatorString(),
      'description_ko' => new sfValidatorString(),
      'is_active'      => new sfValidatorInteger(),
      'is_featured'    => new sfValidatorInteger(),
      'sort'           => new sfValidatorInteger(),
      'created_at'     => new sfValidatorDateTime(),
      'updated_at'     => new sfValidatorDateTime(array('required' => false)),
      'created_aid'    => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Admin'))),
      'updated_aid'    => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Admin_2'))),
    ));

    $this->widgetSchema->setNameFormat('slide[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Slide';
  }

}

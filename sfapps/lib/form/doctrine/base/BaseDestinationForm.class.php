<?php

/**
 * Destination form base class.
 *
 * @method Destination getObject() Returns the current form's model object
 *
 * @package    uaral
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseDestinationForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'title'       => new sfWidgetFormInputText(),
      'title_de'    => new sfWidgetFormInputText(),
      'title_it'    => new sfWidgetFormInputText(),
      'title_ko'    => new sfWidgetFormInputText(),
      'route'       => new sfWidgetFormInputText(),
      'intro'       => new sfWidgetFormInputText(),
      'intro_de'    => new sfWidgetFormInputText(),
      'intro_it'    => new sfWidgetFormInputText(),
      'intro_ko'    => new sfWidgetFormInputText(),
      'overview'    => new sfWidgetFormTextarea(),
      'overview_de' => new sfWidgetFormTextarea(),
      'overview_it' => new sfWidgetFormTextarea(),
      'overview_ko' => new sfWidgetFormTextarea(),
      'youtube'     => new sfWidgetFormInputText(),
      'thumb'       => new sfWidgetFormInputText(),
      'cover'       => new sfWidgetFormInputText(),
      'map'         => new sfWidgetFormInputText(),
      'latitude'    => new sfWidgetFormInputText(),
      'longitude'   => new sfWidgetFormInputText(),
      'offer_ids'   => new sfWidgetFormInputText(),
      'sort'        => new sfWidgetFormInputText(),
      'is_active'   => new sfWidgetFormInputText(),
      'is_featured' => new sfWidgetFormInputText(),
      'created_aid' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Admin'), 'add_empty' => false)),
      'updated_aid' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Admin_2'), 'add_empty' => false)),
      'created_at'  => new sfWidgetFormDateTime(),
      'updated_at'  => new sfWidgetFormDateTime(),
      'nb_views'    => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'          => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'title'       => new sfValidatorString(array('max_length' => 255)),
      'title_de'    => new sfValidatorString(array('max_length' => 255)),
      'title_it'    => new sfValidatorString(array('max_length' => 255)),
      'title_ko'    => new sfValidatorString(array('max_length' => 255)),
      'route'       => new sfValidatorString(array('max_length' => 255)),
      'intro'       => new sfValidatorString(array('max_length' => 255)),
      'intro_de'    => new sfValidatorString(array('max_length' => 255)),
      'intro_it'    => new sfValidatorString(array('max_length' => 255)),
      'intro_ko'    => new sfValidatorString(array('max_length' => 255)),
      'overview'    => new sfValidatorString(),
      'overview_de' => new sfValidatorString(),
      'overview_it' => new sfValidatorString(),
      'overview_ko' => new sfValidatorString(),
      'youtube'     => new sfValidatorString(array('max_length' => 255)),
      'thumb'       => new sfValidatorString(array('max_length' => 255)),
      'cover'       => new sfValidatorString(array('max_length' => 255)),
      'map'         => new sfValidatorString(array('max_length' => 255)),
      'latitude'    => new sfValidatorString(array('max_length' => 255)),
      'longitude'   => new sfValidatorString(array('max_length' => 255)),
      'offer_ids'   => new sfValidatorString(array('max_length' => 255)),
      'sort'        => new sfValidatorInteger(),
      'is_active'   => new sfValidatorInteger(),
      'is_featured' => new sfValidatorInteger(),
      'created_aid' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Admin'))),
      'updated_aid' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Admin_2'))),
      'created_at'  => new sfValidatorDateTime(),
      'updated_at'  => new sfValidatorDateTime(array('required' => false)),
      'nb_views'    => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('destination[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Destination';
  }

}

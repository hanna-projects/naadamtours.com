<?php

/**
 * Item form base class.
 *
 * @method Item getObject() Returns the current form's model object
 *
 * @package    uaral
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseItemForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'page_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Page'), 'add_empty' => false)),
      'title'       => new sfWidgetFormInputText(),
      'title_de'    => new sfWidgetFormInputText(),
      'title_it'    => new sfWidgetFormInputText(),
      'title_ko'    => new sfWidgetFormInputText(),
      'route'       => new sfWidgetFormInputText(),
      'summary'     => new sfWidgetFormInputText(),
      'summary_de'  => new sfWidgetFormInputText(),
      'summary_it'  => new sfWidgetFormInputText(),
      'summary_ko'  => new sfWidgetFormInputText(),
      'content'     => new sfWidgetFormTextarea(),
      'content_de'  => new sfWidgetFormTextarea(),
      'content_it'  => new sfWidgetFormTextarea(),
      'content_ko'  => new sfWidgetFormTextarea(),
      'image1'      => new sfWidgetFormInputText(),
      'sort'        => new sfWidgetFormInputText(),
      'is_active'   => new sfWidgetFormInputText(),
      'is_featured' => new sfWidgetFormInputText(),
      'created_aid' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Admin_2'), 'add_empty' => false)),
      'updated_aid' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Admin'), 'add_empty' => false)),
      'created_at'  => new sfWidgetFormDateTime(),
      'updated_at'  => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'          => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'page_id'     => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Page'))),
      'title'       => new sfValidatorString(array('max_length' => 255)),
      'title_de'    => new sfValidatorString(array('max_length' => 255)),
      'title_it'    => new sfValidatorString(array('max_length' => 255)),
      'title_ko'    => new sfValidatorString(array('max_length' => 255)),
      'route'       => new sfValidatorString(array('max_length' => 255)),
      'summary'     => new sfValidatorString(array('max_length' => 255)),
      'summary_de'  => new sfValidatorString(array('max_length' => 255)),
      'summary_it'  => new sfValidatorString(array('max_length' => 255)),
      'summary_ko'  => new sfValidatorString(array('max_length' => 255)),
      'content'     => new sfValidatorString(),
      'content_de'  => new sfValidatorString(),
      'content_it'  => new sfValidatorString(),
      'content_ko'  => new sfValidatorString(),
      'image1'      => new sfValidatorString(array('max_length' => 255)),
      'sort'        => new sfValidatorInteger(),
      'is_active'   => new sfValidatorInteger(),
      'is_featured' => new sfValidatorInteger(),
      'created_aid' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Admin_2'))),
      'updated_aid' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Admin'))),
      'created_at'  => new sfValidatorDateTime(),
      'updated_at'  => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('item[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Item';
  }

}

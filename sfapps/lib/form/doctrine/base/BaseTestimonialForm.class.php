<?php

/**
 * Testimonial form base class.
 *
 * @method Testimonial getObject() Returns the current form's model object
 *
 * @package    uaral
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseTestimonialForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'text'        => new sfWidgetFormTextarea(),
      'text_de'     => new sfWidgetFormTextarea(),
      'text_it'     => new sfWidgetFormTextarea(),
      'text_ko'     => new sfWidgetFormTextarea(),
      'fullname'    => new sfWidgetFormInputText(),
      'position'    => new sfWidgetFormInputText(),
      'signature'   => new sfWidgetFormInputText(),
      'date'        => new sfWidgetFormDateTime(),
      'is_active'   => new sfWidgetFormInputText(),
      'is_featured' => new sfWidgetFormInputText(),
      'sort'        => new sfWidgetFormInputText(),
      'created_at'  => new sfWidgetFormDateTime(),
      'updated_at'  => new sfWidgetFormDateTime(),
      'created_aid' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Admin'), 'add_empty' => false)),
      'updated_aid' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Admin_2'), 'add_empty' => false)),
    ));

    $this->setValidators(array(
      'id'          => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'text'        => new sfValidatorString(),
      'text_de'     => new sfValidatorString(),
      'text_it'     => new sfValidatorString(),
      'text_ko'     => new sfValidatorString(),
      'fullname'    => new sfValidatorString(array('max_length' => 255)),
      'position'    => new sfValidatorString(array('max_length' => 255)),
      'signature'   => new sfValidatorString(array('max_length' => 255)),
      'date'        => new sfValidatorDateTime(array('required' => false)),
      'is_active'   => new sfValidatorInteger(),
      'is_featured' => new sfValidatorInteger(),
      'sort'        => new sfValidatorInteger(),
      'created_at'  => new sfValidatorDateTime(),
      'updated_at'  => new sfValidatorDateTime(array('required' => false)),
      'created_aid' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Admin'))),
      'updated_aid' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Admin_2'))),
    ));

    $this->widgetSchema->setNameFormat('testimonial[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Testimonial';
  }

}

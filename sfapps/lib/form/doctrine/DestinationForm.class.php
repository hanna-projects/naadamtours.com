<?php

/**
 * Destination form.
 *
 * @package    uaral
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class DestinationForm extends BaseDestinationForm
{
  public function configure()
  {
  		$host = sfConfig::get('app_host');
      unset($this['offer_ids']);
  		
      // WIDGETS		
      $this->widgetSchema['title']       						= new sfWidgetFormInputText(array(), array());
      $this->widgetSchema['title_de']    						= new sfWidgetFormInputText(array(), array());
      $this->widgetSchema['title_it']    						= new sfWidgetFormInputText(array(), array());
      $this->widgetSchema['title_ko']   			 			= new sfWidgetFormInputText(array(), array());
	  	$this->widgetSchema['intro']     					 		= new sfWidgetFormTextarea(array(), array());
      $this->widgetSchema['intro_de'] 	 						= new sfWidgetFormTextarea(array(), array());
      $this->widgetSchema['intro_it']  	 						= new sfWidgetFormTextarea(array(), array());
      $this->widgetSchema['intro_ko']  	 						= new sfWidgetFormTextarea(array(), array());	  	
      $this->widgetSchema['overview']    						= new sfWidgetFormTextarea(array(), array());
      $this->widgetSchema['overview_de'] 						= new sfWidgetFormTextarea(array(), array());
      $this->widgetSchema['overview_it'] 						= new sfWidgetFormTextarea(array(), array());
      $this->widgetSchema['overview_ko'] 						= new sfWidgetFormTextarea(array(), array());
      
      $this->widgetSchema['thumb']       			   		= new sfWidgetFormInputFile(array(), array());
      $this->widgetSchema['cover']      				 		= new sfWidgetFormInputFile(array(), array());
      $this->widgetSchema['map']        				 		= new sfWidgetFormInputFile(array(), array());
			$this->widgetSchema['youtube']  				 			= new sfWidgetFormInputText(array(), array());
			$this->widgetSchema['latitude']  				 	 		= new sfWidgetFormInputText(array(), array());
			$this->widgetSchema['longitude']  					  = new sfWidgetFormInputText(array(), array());

      // VALIDATORS
      $this->validatorSchema['title']   	  				= new sfValidatorString();
      $this->validatorSchema['title_de']    				= new sfValidatorString();
      $this->validatorSchema['title_it']    				= new sfValidatorString();
      $this->validatorSchema['title_ko']    				= new sfValidatorString();
      $this->validatorSchema['intro']	      				= new sfValidatorPass();
      $this->validatorSchema['intro_de']    				= new sfValidatorPass();
      $this->validatorSchema['intro_it']    				= new sfValidatorPass();
      $this->validatorSchema['intro_ko']    				= new sfValidatorPass();
	  	$this->validatorSchema['overview']	  				= new sfValidatorPass();
      $this->validatorSchema['overview_de'] 				= new sfValidatorPass();
      $this->validatorSchema['overview_it'] 				= new sfValidatorPass();
      $this->validatorSchema['overview_ko'] 				= new sfValidatorPass();
      
      $this->validatorSchema['thumb']      				  = new sfValidatorFile($this->getFileAttrs('dest'), $this->getFileOpts());
      $this->validatorSchema['cover']      				  = new sfValidatorFile($this->getFileAttrs('dest'), $this->getFileOpts());
      $this->validatorSchema['map']     	 					= new sfValidatorFile($this->getFileAttrs('dest'), $this->getFileOpts());
      $this->validatorSchema['youtube'] 					  = new sfValidatorPass();
      $this->validatorSchema['latitude'] 					  = new sfValidatorPass();
      $this->validatorSchema['longitude'] 		      = new sfValidatorPass();
  }
}

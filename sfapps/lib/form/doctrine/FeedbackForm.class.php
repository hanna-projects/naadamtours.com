<?php

/**
 * Feedback form.
 *
 * @package    imdb
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class FeedbackForm extends BaseFeedbackForm
{
  public function configure()
  {
		unset($this['county'], $this['state'], $this['city'], $this['ip_address'], 
					$this['seen_at'], $this['last_replied_at'], $this['response']);

		$choices = GlobalLib::getArray('enquiry');
		# fields
		$this->widgetSchema['title']     			= new sfWidgetFormChoice(array('choices' => $choices['title']), array('id'=>'enquiry_title', 'style'=>'width:45px;height:39px;'));
		$this->widgetSchema['firstname']  			= new sfWidgetFormInputText(array(), array('id'=>'enquiry_firstname', 'style'=>'width:166px', 'placeholder'=>'First name'));
		$this->widgetSchema['lastname']   			= new sfWidgetFormInputText(array(), array('id'=>'enquiry_lastname', 'style'=>'width:167px', 'placeholder'=>'Last name'));
		$this->widgetSchema['email']      			= new sfWidgetFormInputText(array(), array('id'=>'enquiry_email', 'style'=>'width:400px', 'placeholder'=>'email@domain.com', 'type'=>'email'));
		$this->widgetSchema['phone']     			= new sfWidgetFormInputText(array(), array('id'=>'enquiry_phone', 'style'=>'width:400px', 'placeholder'=>'Phone number'));
		$this->widgetSchema['besttime2call'] 		= new sfWidgetFormChoice(array('choices' => $choices['besttime2call']), array('value'=>'Can we Skype?', 'id'=>'enquiry_besttime2call', 'style'=>'width:409px;height:39px;'));

		$this->widgetSchema['enquiry']   			= new sfWidgetFormTextarea(array(), array('id'=>'enquiry_enquiry', 'style'=>'width:400px;height:100px;', 'placeholder'=>'What would you like to know about?'));
		$this->widgetSchema['motivation']    		= new sfWidgetFormChoice(array('choices' => $choices['motivation'], 'multiple'=>true, 'expanded'=>true), array('value'=>'Motivation', 'id'=>'enquiry_motivation', 'style'=>'clear:both;'));
		$this->widgetSchema['accommodation'] 		= new sfWidgetFormChoice(array('choices' => $choices['accommodation'], 'multiple'=>true, 'expanded'=>true), array('value'=>'Accommodation', 'id'=>'enquiry_accommodation', 'style'=>'float:left;margin:2px 7px 0 0;cursor:pointer;'));
		
		# validators
		$this->validatorSchema['title'] 			= new sfValidatorString(array(), array());
		$this->validatorSchema['firstname'] 		= new sfValidatorString(array(), array());
		$this->validatorSchema['lastname'] 			= new sfValidatorString(array(), array());
		$this->validatorSchema['email']     		= new sfValidatorEmail(array(), array());
		$this->validatorSchema['phone']  			= new sfValidatorString(array(), array());
		$this->validatorSchema['besttime2call'] 	= new sfValidatorString(array(), array());
		$this->validatorSchema['enquiry'] 			= new sfValidatorString(array(), array());
		$this->validatorSchema['motivation'] 		= new sfValidatorString(array(), array());
		$this->validatorSchema['accommodation'] 	= new sfValidatorString(array(), array());
		
  }
}

<?php

/**
 * Image form.
 *
 * @package    uaral
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ImageForm extends BaseImageForm
{
  public function configure()
  {
        # WIDGETS
				$this->widgetSchema['object_id']      = new sfWidgetFormInputHidden(array(), array());
				$this->setDefault('object_id', $this->getOption('objectId'));
				$this->widgetSchema['object_type']    = new sfWidgetFormInputHidden(array(), array());
				$this->setDefault('object_type', $this->getOption('objectType'));
				$this->widgetSchema['filename']       = new sfWidgetFormInputFile(array(), array());
				$this->widgetSchema['title']          = new sfWidgetFormInputText(array(), array());
				$this->widgetSchema['title_de']    	  = new sfWidgetFormInputText(array(), array());
				$this->widgetSchema['title_it']    	  = new sfWidgetFormInputText(array(), array());
				$this->widgetSchema['title)ko']    	  = new sfWidgetFormInputText(array(), array());
				$this->widgetSchema['description'] 	  = new sfWidgetFormTextarea(array(), array());     
				$this->widgetSchema['description_de'] = new sfWidgetFormTextarea(array(), array());     
				$this->widgetSchema['description_it'] = new sfWidgetFormTextarea(array(), array());     
				$this->widgetSchema['description_ko'] = new sfWidgetFormTextarea(array(), array());     
      
        # VALIDATORS
	    	$this->validatorSchema['object_id']      = new sfValidatorInteger();
	    	$this->validatorSchema['object_type']    = new sfValidatorString();
				$this->validatorSchema['filename']     	 = new sfValidatorFile($this->getFileAttrs('images'), $this->getFileOpts());
	    	$this->validatorSchema['title'] 	     	 = new sfValidatorPass();
	    	$this->validatorSchema['title_de'] 	     = new sfValidatorPass();
	    	$this->validatorSchema['title_it'] 	     = new sfValidatorPass();
	    	$this->validatorSchema['title_ko'] 	     = new sfValidatorPass();
	    	$this->validatorSchema['description'] 	 = new sfValidatorPass();
	    	$this->validatorSchema['description_de'] = new sfValidatorPass();
	    	$this->validatorSchema['description_it'] = new sfValidatorPass();
	    	$this->validatorSchema['description_ko'] = new sfValidatorPass();
  }
  
}
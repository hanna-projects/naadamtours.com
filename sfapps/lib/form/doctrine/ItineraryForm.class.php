<?php

/**
 * Itinerary form.
 *
 * @package    uaral
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ItineraryForm extends BaseItineraryForm
{
  public function configure()
  {
  		$host = sfConfig::get('app_host');
      
      // WIDGETS		
      $this->widgetSchema['offer_id']       				= new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Offer'), 'add_empty' => false));
      $this->widgetSchema['day']                    = new sfWidgetFormChoice(array('choices'=>GlobalLib::getNumbers(1, 30)), array());
      $this->widgetSchema['title']       						= new sfWidgetFormInputText(array(), array());
      $this->widgetSchema['title_de']    						= new sfWidgetFormInputText(array(), array());
      $this->widgetSchema['title_it']    						= new sfWidgetFormInputText(array(), array());
      $this->widgetSchema['title_ko']   			 			= new sfWidgetFormInputText(array(), array());
	  	$this->widgetSchema['content']     					 	= new sfWidgetFormTextarea(array(), array());
      $this->widgetSchema['content_de'] 	 					= new sfWidgetFormTextarea(array(), array());
      $this->widgetSchema['content_it']  	 					= new sfWidgetFormTextarea(array(), array());
      $this->widgetSchema['content_ko']  	 					= new sfWidgetFormTextarea(array(), array());	  	
      $this->widgetSchema['image']       			   		= new sfWidgetFormInputFile(array(), array());
			$this->widgetSchema['meals']  				 				= new sfWidgetFormInputText(array(), array());
			
			$this->setDefault('offer_id', intval($this->getOption('offerId')));
			$this->setDefault('meals', 'B L D');

      // VALIDATORS
      $this->validatorSchema['offer_id']   	  			= new sfValidatorInteger();
      $this->validatorSchema['day']   	  			    = new sfValidatorInteger();
      $this->validatorSchema['title']   	  				= new sfValidatorString();
      $this->validatorSchema['title_de']    				= new sfValidatorPass();
      $this->validatorSchema['title_it']    				= new sfValidatorPass();
      $this->validatorSchema['title_ko']    				= new sfValidatorPass();
      $this->validatorSchema['content']	      			= new sfValidatorPass();
      $this->validatorSchema['content_de']    			= new sfValidatorPass();
      $this->validatorSchema['content_it']    			= new sfValidatorPass();
      $this->validatorSchema['content_ko']    			= new sfValidatorPass();
      $this->validatorSchema['image']      				  = new sfValidatorFile($this->getFileAttrs('itinerary'), $this->getFileOpts());
      $this->validatorSchema['meals'] 					  	= new sfValidatorPass();
  }
}
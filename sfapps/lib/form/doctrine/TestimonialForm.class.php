<?php

/**
 * Testimonial form.
 *
 * @package    uaral
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class TestimonialForm extends BaseTestimonialForm
{
  public function configure()
  {
  			# WIDGETS				
				$this->widgetSchema['text']           = new sfWidgetFormTextarea(array(), array());
				$this->widgetSchema['text_de']    	  = new sfWidgetFormTextarea(array(), array());
				$this->widgetSchema['text_it']    	  = new sfWidgetFormTextarea(array(), array());
				$this->widgetSchema['text_ko']    	  = new sfWidgetFormTextarea(array(), array());
				$this->widgetSchema['fullname']       = new sfWidgetFormInputText(array(), array());
				$this->widgetSchema['position']       = new sfWidgetFormInputText(array(), array());
				$this->widgetSchema['date']           = new sfWidgetFormDate(array(), array('style'=>'width:60px;'));
				$this->widgetSchema['signature']      = new sfWidgetFormInputFile(array(), array());
      
        # VALIDATORS	    	
	    	$this->validatorSchema['text'] 	    	 = new sfValidatorString();
	    	$this->validatorSchema['text_de'] 	   = new sfValidatorString();
	    	$this->validatorSchema['text_it'] 	   = new sfValidatorString();
	    	$this->validatorSchema['text_ko'] 	   = new sfValidatorString();
	    	$this->validatorSchema['fullname'] 	   = new sfValidatorString();
	    	$this->validatorSchema['position'] 	   = new sfValidatorPass();
	    	$this->validatorSchema['date'] 	  		 = new sfValidatorDate();
	    	$this->validatorSchema['signature']    = new sfValidatorFile($this->getFileAttrs('testimonial'), $this->getFileOpts());
  }
}

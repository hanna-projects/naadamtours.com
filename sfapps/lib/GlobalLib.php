<?php

class GlobalLib
{
    public static $banner_position = array(
                  'top'          =>'Top 750 x 100', 
                  'left'         =>'Left 150 x 300', 
                  'right-top'    =>'Right top 160 x 300',
                  'right-middle' =>'Right middle 160 x 300', 
                  'right-bottom' =>'Right bottom 160 x 300', 
                  'footer'       =>'Footer 1100 x 100'
    );

    public static $mod_permissions = array(
									'admin'=>'admin', 'dest'=>'dest', 'feedback'=>'feedback', 
    							'image'=>'image', 'itinerary'=>'itinerary', 'item'=>'item', 
    							'offer'=>'offer', 'page'=>'page',
                  'slide'=>'slide', 'subitem'=>'subitem', 'subscriber'=>'subscriber', 'testimonial'=>'testimonial');

		public static $enquiry = array(
				'title'					=> array('mr'=>'Mr', 'mrs'=>'Mrs', 'miss'=>'Miss', 'ms'=>'Ms', 'dr'=>'Dr'), 
				'besttime2call' => array('best'=>'Best time to call to?', '9-10'=>'9am - 10am', '10-11'=>'10am - 11am', '11-12'=>'11am - 12am', 
																 '12-1'=>'12pm - 1pm', '1-2'=>'1pm - 2pm', '2-3'=>'2pm - 3pm', '3-4'=>'3pm - 4pm', '4-5'=>'4pm - 5pm', 
									 							 '5-6'=>'5pm - 6pm', 'evenings'=>'Evenings'),
				'motivation'    => array(1 => 'See a new country and experience a new culture',
																 2 => 'Rest, relax and recharge',
																 3 => 'Pampering and indulgence',
																 4 => 'Spend quality time with your partner',
																 5 => 'Family bonding',
																 6 => 'Exercise, get fit and eat healthily',
																 7 => 'Celebrating something – e.g. wedding anniversary, family reunion'), 
				'accommodation'				=> array(1 => 'Luxury',
																			 2 => 'Heritage',
																			 3 => 'Boutique',
																			 4 => 'Contemporary',
																			 5 => 'Quirky/Charming',
																			 6 => 'Hideaways/Escapes'),
				'specialist_interest'	=> array(1 => 'Architecture',
																			 2 => 'Art',
																			 3 => 'History',
																			 4 => 'Temples',
																			 5 => 'Food',
																			 6 => 'Shopping',
																			 7 => 'Nature & Wildlife',
																			 8 => 'Photography',
																			 9 => 'Wellness & spirituality'),
				'guiding'							=> array(1 => 'Hands-on',
																			 2 => 'Medium',
																			 3 => 'Hands-off',
																			 4 => 'No guide'),
				'hear_source'					=> array(1 => 'Google search',
																			 2 => 'Google advert',
																			 3 => 'Newspaper, magazine or online article',
																			 4 => 'Website or Blog',
																			 5 => 'Recommended by a friend',
																			 6 => 'I\'ve travelled with Porta Travel before',
																			 7 => 'Facebook, Twitter or Pinterest',
																			 8 => 'Porta Travel Newsletter',
																			 9 => 'Other')
		);
                  
                  
    public static function getArray($type)
    {
        switch ($type) {
          	case 'mod_permissions': return self::$mod_permissions;
          	case 'banner_position': return self::$banner_position;
          	case 'enquiry': return self::$enquiry;
        }
       return array();
    }
    
    public static function getValues($type)
    {
        return array_values(self::getArray($type));
    }    
    
    public static function getKeys($type)
    {
        return array_keys(self::getArray($type));
    }
    
    public static function getValue($type, $key)
    {
        $array = self::getArray($type);
        return $array[$key];
    }
    
    

    ################################################################################################################################################################
    
    public static function getNumbers($min=1, $max=1000)
    {
        $a = array();
        for($i=$min; $i <= $max; $i++) {
            $a[$i] = $i;
        }
        return $a;
    }
    
    static public function slugify($text)
    {
        // replace all non letters or digits by -
        $text = preg_replace('/\W+/', '-', $text);
     
        // trim and lowercase
        $text = strtolower(trim($text, '-'));
     
        return $text;
    }
    
    
  
    public static function getFileName($fileName) {
        try {
          return substr($fileName, strrpos($fileName,'/')+1,strlen($fileName)-strrpos($fileName,'/'));
        }catch (Exception $e) {
    
        }
        return ;
    }
  
    public static function getFileExtension($fileName) {
        return strtolower(substr(strrchr($fileName, '.'), 1));
    }
  
    public static function get3FileExtension($fileName) {
        $ext = strtolower(substr(strrchr($fileName, '.'), 1));
        if(in_array($ext, array('doc','docx','xls','xlsx','pdf','pdfx'))) {
          return $ext;
        }
        return 'pdf';
    }
    
    
    public static function mn2en($st) {
        $replacement = array(
                "й"=>"i","ц"=>"ts","у"=>"u","к"=>"k","е"=>"ye","н"=>"n",'ү'=>'u',
                "г"=>"g","ш"=>"sh","щ"=>"sh","з"=>"z","х"=>"h","ъ"=>"-",'ө'=>'u',
                "ф"=>"f","ы"=>"ii","в"=>"v","а"=>"a","п"=>"p","р"=>"r",
                "о"=>"o","л"=>"l","д"=>"d","ж"=>"j","э"=>"e","ё"=>"yo",
                "я"=>"ya","ч"=>"ch","с"=>"s","м"=>"m","и"=>"i","т"=>"t",
                "ь"=>"-","б"=>"b","ю"=>"yu","\""=>"-","'"=>"-",
                "Й"=>"I","Ц"=>"TS","У"=>"U","К"=>"K","Е"=>"YE","Н"=>"N",'Ү'=>'U',
                "Г"=>"G","Ш"=>"SH","Щ"=>"SH","З"=>"Z","Х"=>"H","Ъ"=>"-",'Ө'=>'U',
                "Ф"=>"F","Ы"=>"II","В"=>"V","А"=>"A","П"=>"P","Р"=>"R",
                "О"=>"O","Л"=>"L","Д"=>"D","Ж"=>"J","Э"=>"E","Ё"=>"YO",
                "Я"=>"YA","Ч"=>"CH","С"=>"S","М"=>"M","И"=>"I","Т"=>"T",
                "Ь"=>"-'","Б"=>"B","Ю"=>"YU",
        );
    
        foreach($replacement as $i=>$u) {
            $st = mb_eregi_replace($i,$u,$st);
        }
        return $st;
    }
    
    public static function createThumbs($filename, $folder, $thumbs=array(), $removeOrg=false) 
    {
        try {
            $ext = GlobalLib::getFileExtension($filename);
            if(in_array($ext, array('jpg', 'jpeg', 'png', 'gif', 'JPG', 'JPEG', 'PNG', 'GIF'))) 
            {
                $uploadDir = sfConfig::get('sf_upload_dir').'/'.$folder.'/';
                if (sizeof($thumbs) && $filename && file_exists($uploadDir.$filename)) {
                    // create thumbs
                    foreach ($thumbs as $thumb) {
                        $thumbnail = new sfThumbnail($thumb, null, true, false, 100);
                        $thumbnail->loadFile($uploadDir.$filename);
                        $thumbnail->save($uploadDir."t".$thumb."-".$filename);
                    }
                    // remove orig image
                    if($removeOrg) unlink($uploadDir.$filename);
                    return true;
                }
            } // not an image file
        }catch (Exception $e) {
    
        }
        return false;
    }  
    
    public static function createQualities($filename, $folder, $qualities=array(), $removeOrg=false) 
    {
        try {
            $ext = GlobalLib::getFileExtension($filename);
            if(in_array($ext, array('jpg', 'jpeg', 'png', 'gif', 'JPG', 'JPEG', 'PNG', 'GIF'))) 
            {
                $uploadDir = sfConfig::get('sf_upload_dir').'/'.$folder.'/';
                if (sizeof($qualities) && $filename && file_exists($uploadDir.$filename)) {
                    // create quilities
                    foreach ($qualities as $quality) {
                        $thumbnail = new sfThumbnail(null, null, true, false, $quality);
                        $thumbnail->loadFile($uploadDir.$filename);
                        $thumbnail->save($uploadDir."q".$quality."-".$filename);
                    }
                    // remove orig image
                    if($removeOrg) unlink($uploadDir.$filename);
                    return true;
                }
            } // not an image file
        }catch (Exception $e) {
    
        }
        return false;
    }
    
    public static function clearOutput($text)
    {
        return nl2br(htmlspecialchars_decode($text));
        //return nl2br(htmlspecialchars_decode(strip_tags($text, '<strong><b><i><em><u><sup><sub><ol><ul><li><a><img><embed><object><iframe>')));
    }
    
    public static function clearInput($text)
    {
        // xss, html tags are escaped here?
        $text = preg_replace('/\?/', ' ', $text);
        $text = preg_replace('/\"/', ' ', $text);
        $text = preg_replace('/\'/', ' ', $text);
        $text = preg_replace('/</', ' ', $text);
        $text = preg_replace('/>/', ' ', $text);
        
        // strip tags
        $text = htmlspecialchars_decode($text);
        return $text;
    }

}

?>
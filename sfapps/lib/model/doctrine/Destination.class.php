<?php

/**
 * Destination
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    uaral
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class Destination extends BaseDestination
{
	public function __toString() {
		$cul = ProjectConfiguration::getApplicationConfiguration('front', 'prod', false)->loadHelpers('I18N');
		if($cul == 'de') {
			return $this->getTitleDe();
		} elseif($cul == 'it') {
			return $this->getTitleIt();
		} elseif($cul == 'ko') {
			return $this->getTitleKo();
		} else {
			return $this->getTitle();
		}
	}
	
	public function getIntroCulture() {
		$cul = ProjectConfiguration::getApplicationConfiguration('front', 'prod', false)->loadHelpers('I18N');
		if($cul == 'de') {
			return $this->getIntroDe();
		} elseif($cul == 'it') {
			return $this->getIntroIt();
		} elseif($cul == 'ko') {
			return $this->getIntroKo();
		} else {
			return $this->getIntro();
		}
	}
	
	public function getOverviewCulture() {
		$cul = ProjectConfiguration::getApplicationConfiguration('front', 'prod', false)->loadHelpers('I18N');
		if($cul == 'de') {
			return $this->getOverviewDe();
		} elseif($cul == 'it') {
			return $this->getOverviewIt();
		} elseif($cul == 'ko') {
			return $this->getOverviewKo();
		} else {
			return $this->getOverview();
		}
	}
}
<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('Testimonial', 'doctrine');

/**
 * BaseTestimonial
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property string $text
 * @property string $text_de
 * @property string $text_it
 * @property string $text_ko
 * @property string $fullname
 * @property string $position
 * @property string $signature
 * @property timestamp $date
 * @property integer $is_active
 * @property integer $is_featured
 * @property integer $sort
 * @property timestamp $created_at
 * @property timestamp $updated_at
 * @property integer $created_aid
 * @property integer $updated_aid
 * @property Admin $Admin
 * @property Admin $Admin_2
 * 
 * @method integer     getId()          Returns the current record's "id" value
 * @method string      getText()        Returns the current record's "text" value
 * @method string      getTextDe()      Returns the current record's "text_de" value
 * @method string      getTextIt()      Returns the current record's "text_it" value
 * @method string      getTextKo()      Returns the current record's "text_ko" value
 * @method string      getFullname()    Returns the current record's "fullname" value
 * @method string      getPosition()    Returns the current record's "position" value
 * @method string      getSignature()   Returns the current record's "signature" value
 * @method timestamp   getDate()        Returns the current record's "date" value
 * @method integer     getIsActive()    Returns the current record's "is_active" value
 * @method integer     getIsFeatured()  Returns the current record's "is_featured" value
 * @method integer     getSort()        Returns the current record's "sort" value
 * @method timestamp   getCreatedAt()   Returns the current record's "created_at" value
 * @method timestamp   getUpdatedAt()   Returns the current record's "updated_at" value
 * @method integer     getCreatedAid()  Returns the current record's "created_aid" value
 * @method integer     getUpdatedAid()  Returns the current record's "updated_aid" value
 * @method Admin       getAdmin()       Returns the current record's "Admin" value
 * @method Admin       getAdmin2()      Returns the current record's "Admin_2" value
 * @method Testimonial setId()          Sets the current record's "id" value
 * @method Testimonial setText()        Sets the current record's "text" value
 * @method Testimonial setTextDe()      Sets the current record's "text_de" value
 * @method Testimonial setTextIt()      Sets the current record's "text_it" value
 * @method Testimonial setTextKo()      Sets the current record's "text_ko" value
 * @method Testimonial setFullname()    Sets the current record's "fullname" value
 * @method Testimonial setPosition()    Sets the current record's "position" value
 * @method Testimonial setSignature()   Sets the current record's "signature" value
 * @method Testimonial setDate()        Sets the current record's "date" value
 * @method Testimonial setIsActive()    Sets the current record's "is_active" value
 * @method Testimonial setIsFeatured()  Sets the current record's "is_featured" value
 * @method Testimonial setSort()        Sets the current record's "sort" value
 * @method Testimonial setCreatedAt()   Sets the current record's "created_at" value
 * @method Testimonial setUpdatedAt()   Sets the current record's "updated_at" value
 * @method Testimonial setCreatedAid()  Sets the current record's "created_aid" value
 * @method Testimonial setUpdatedAid()  Sets the current record's "updated_aid" value
 * @method Testimonial setAdmin()       Sets the current record's "Admin" value
 * @method Testimonial setAdmin2()      Sets the current record's "Admin_2" value
 * 
 * @package    uaral
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseTestimonial extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('testimonial');
        $this->hasColumn('id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => true,
             'autoincrement' => true,
             'length' => 4,
             ));
        $this->hasColumn('text', 'string', null, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => '',
             ));
        $this->hasColumn('text_de', 'string', null, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => '',
             ));
        $this->hasColumn('text_it', 'string', null, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => '',
             ));
        $this->hasColumn('text_ko', 'string', null, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => '',
             ));
        $this->hasColumn('fullname', 'string', 255, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 255,
             ));
        $this->hasColumn('position', 'string', 255, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 255,
             ));
        $this->hasColumn('signature', 'string', 255, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 255,
             ));
        $this->hasColumn('date', 'timestamp', 25, array(
             'type' => 'timestamp',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'default' => '0000-00-00 00:00:00',
             'notnull' => true,
             'autoincrement' => false,
             'length' => 25,
             ));
        $this->hasColumn('is_active', 'integer', 1, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 1,
             ));
        $this->hasColumn('is_featured', 'integer', 1, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 1,
             ));
        $this->hasColumn('sort', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 4,
             ));
        $this->hasColumn('created_at', 'timestamp', 25, array(
             'type' => 'timestamp',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 25,
             ));
        $this->hasColumn('updated_at', 'timestamp', 25, array(
             'type' => 'timestamp',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'default' => '0000-00-00 00:00:00',
             'notnull' => true,
             'autoincrement' => false,
             'length' => 25,
             ));
        $this->hasColumn('created_aid', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 4,
             ));
        $this->hasColumn('updated_aid', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 4,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('Admin', array(
             'local' => 'created_aid',
             'foreign' => 'id'));

        $this->hasOne('Admin as Admin_2', array(
             'local' => 'updated_aid',
             'foreign' => 'id'));
    }
}
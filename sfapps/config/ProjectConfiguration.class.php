<?php

require_once 'C:/Bitnami/wampstack-7.1.14-0/apache2/htdocs/naadamtours.com/vendor/symfony14//lib/autoload/sfCoreAutoload.class.php';
#require_once '/home/tumenjargal/public_html/symfony14/lib/autoload/sfCoreAutoload.class.php';

sfCoreAutoload::register();

class ProjectConfiguration extends sfProjectConfiguration
{
  public function setup()
  {
      $this->enablePlugins('sfDoctrinePlugin', 'sfThumbnailPlugin');
      sfConfig::set('sf_web_dir', 'C:\Bitnami\wampstack-7.1.14-0\apache2\htdocs\naadamtours.com');
      sfConfig::set('sf_upload_dir', 'C:\Bitnami\wampstack-7.1.14-0\apache2\htdocs\naadamtours.com\u');
      sfConfig::set('rich_text_fck_js_dir', 'C:\Bitnami\wampstack-7.1.14-0\apache2\htdocs\naadamtours.com\js');
	  
	  #sfConfig::set('sf_web_dir', '/home/tumenjargal/public_html');
      #sfConfig::set('sf_upload_dir', '/home/tumenjargal/public_html/u');
      #sfConfig::set('rich_text_fck_js_dir', '/home/tumenjargal/public_html/js');
  }

}

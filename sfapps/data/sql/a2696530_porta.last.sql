-- phpMyAdmin SQL Dump
-- version 2.11.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 06, 2016 at 01:57 AM
-- Server version: 5.1.57
-- PHP Version: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `a2696530_porta`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mod_permissions` text COLLATE utf8_unicode_ci,
  `cat_permissions` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logged_at` datetime NOT NULL,
  `sort` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_featured` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_aid` int(11) NOT NULL,
  `updated_aid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` VALUES(1, 'handaa.1224@gmail.com', '11611531cc390b3d047142569042fca3', 'admin;feedback;image;item;page;slide;subitem;subscriber;testimonial', '', '0000-00-00 00:00:00', 0, 1, 0, '2011-02-03 19:04:13', '2015-12-04 16:58:17', 0, 0);
INSERT INTO `admin` VALUES(2, 'duuya2012@gmail.com', '11611531cc390b3d047142569042fca3', 'admin;feedback;image;item;page;slide;subitem;subscriber;testimonial', '', '0000-00-00 00:00:00', 0, 1, 0, '2011-02-03 19:04:13', '2015-12-04 16:58:36', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `besttime2call` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enquiry` text COLLATE utf8_unicode_ci NOT NULL,
  `motivation` text COLLATE utf8_unicode_ci NOT NULL,
  `accommodation` text COLLATE utf8_unicode_ci NOT NULL,
  `specialist_interest` text COLLATE utf8_unicode_ci NOT NULL,
  `guiding` text COLLATE utf8_unicode_ci NOT NULL,
  `hear_source` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `seen_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `response` text COLLATE utf8_unicode_ci NOT NULL,
  `last_replied_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_aid` int(11) NOT NULL,
  `updated_aid` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `created_aid` (`created_aid`),
  KEY `updated_aid` (`updated_aid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `feedback`
--


-- -------
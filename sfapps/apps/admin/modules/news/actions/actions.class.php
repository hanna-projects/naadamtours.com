<?php

/**
 * news actions.
 *
 * @package    zzz
 * @subpackage news
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class newsActions extends sfActions
{

  public function executePerm() {
      $this->getUser()->setFlash('flash', 'Permission denied!', true);
  }

  public function executeIndex(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('news'), 'news', 'perm');
      $params = array();
      $params['isActive'] = 'all';
      if($request->getParameter('s')) $params['s'] = $request->getParameter('s');
      $this->pager = GlobalTable::getPager('News', array('*'), $params);
  }

  public function executeNew(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('news'), 'news', 'perm');
      $this->form = new NewsForm();
      $this->setTemplate('edit');
  }
  
  public function executeCreate(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('news'), 'news', 'perm');
      $this->forward404Unless($request->isMethod(sfRequest::POST));
      $this->form = new NewsForm();
      $this->processForm($request, $this->form);
      $this->setTemplate('edit');
  }

  public function executeEdit(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('news'), 'news', 'perm');
      $this->forward404Unless($news = Doctrine::getTable('News')->find(array($request->getParameter('id'))), sprintf('Object news does not exist (%s).', $request->getParameter('id')));
      $this->form = new NewsForm($news);
  }

  public function executeUpdate(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('news'), 'news', 'perm');
      $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
      $this->forward404Unless($news = Doctrine::getTable('News')->find(array($request->getParameter('id'))), sprintf('Object news does not exist (%s).', $request->getParameter('id')));
      $this->form = new NewsForm($news);
      $this->processForm($request, $this->form);
      $this->setTemplate('edit');
  }
  
  public function executeDelete(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('news'), 'news', 'perm');
      $this->forward404Unless($news = Doctrine::getTable('News')->find(array($request->getParameter('id'))), sprintf('Object news does not exist (%s).', $request->getParameter('id')));
      try {
          $news->delete();
          $this->getUser()->setFlash('flash', 'Successfully deleted.', true);
      } catch (Exception  $e){}
      $this->redirect('news/index');
  }

  public function executeActivate(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('news'), 'news', 'perm');
      
      $this->forward404Unless($news = Doctrine::getTable('News')->find($request->getParameter('id')));
      $this->forward404Unless(in_array($cmd = $request->getParameter('cmd'), array(0,1)));
      $news->setIsActive($cmd);
      $news->save();
      $this->getUser()->setFlash('flash', 'Successfully saved.', true);
      $this->redirect('news/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form) {
      $this->forwardUnless($this->getUser()->hasCredential('news'), 'news', 'perm');
      $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
      if ($form->isValid()) {
          $news = $form->save();
          $news->setUpdatedAt(date('Y-m-d H:i:s'));
          $news->save();
    
          $this->getUser()->setFlash('flash', 'Successfully saved.', true);
          $this->redirect('news/index');
      }
  }


}
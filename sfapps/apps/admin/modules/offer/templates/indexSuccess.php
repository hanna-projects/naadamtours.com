<form action="<?php echo url_for('offer/index')?>" method="GET">
  <?php include_partial('partial/search', array());?>
</form>
<br clear="all">
<br clear="all">
<table width="100%">
  <thead>
    <tr>
      <th>#</th>
      <th>Manage</th>
      <th>Title</th>
      <th>De</th>
      <th>It</th>
      <th>Ko</th>
      <th>Thumb</th>
      <th>Cover</th>
      <th>Map</th>
      <th>Status</th>
      <th>Sort</th>
      <th>Date</th>
      <th>Admin</th>
    </tr>
  </thead>
  <tbody>
		<?php $i=0; foreach ($pager->getResults() as $rs): ?>
		<tr style="background:<?php if(!$rs->getIsActive()) echo '#dedede;'?>">
		  <td><?php echo ++$i?></td>
		  <td nowrap>
			  <?php include_partial('partial/editDelete', array('module'=>'offer', 'id'=>$rs->getId()));?><br>
			  <?php include_partial('partial/isActive', array('module'=>'offer', 'rs'=>$rs));?>
			  <a href="<?php echo url_for('image/new?objectType=offer&objectId='.$rs->getId())?>" title="Images" class="action">Images</a> | 
			  <a href="<?php echo url_for('itinerary/index?offerId='.$rs->getId())?>" title="Itinerary" class="action">Itinerary</a>
		  </td>
		  <td><a href="<?php echo url_for('offer/edit?id='.$rs->getId())?>"><?php echo $rs->getTitle() ?></a></td>
		  <td><?php echo $rs->getTitleDe() ?></td>
		  <td><?php echo $rs->getTitleIt() ?></td>
		  <td><?php echo $rs->getTitleKo() ?></td>
		  <td><?php if($rs->getThumb()) echo image_tag('/u/offer/'.$rs->getThumb(), array('style'=>'max-width:200px;')) ?></td>
		  <td><?php if($rs->getCover()) echo image_tag('/u/offer/'.$rs->getCover(), array('style'=>'max-width:200px;')) ?></td>
		  <td><?php if($rs->getMap()) echo image_tag('/u/offer/'.$rs->getMap(), array('style'=>'max-width:200px;')) ?></td>
		  <?php include_partial('partial/td_active_featured', array('rs'=>$rs));?>
		  <?php include_partial('partial/td_sort_date_admin', array('rs'=>$rs));?>		  
		</tr>
		<?php endforeach; ?>
  </tbody>
</table>
<br clear="all">
<?php echo pager($pager, 'offer/index?s='.$sf_params->get('s'))?>

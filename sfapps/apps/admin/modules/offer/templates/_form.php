<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<?php $object = $form->getObject() ?>
<form action="<?php echo url_for('offer/'.($object->isNew() ? 'create' : 'update').(!$object->isNew() ? '?id='.$object->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
<?php if (!$object->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>

  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          <?php echo $form->renderHiddenFields(false) ?>
          <input type="submit" value="Save" class="button" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><?php echo $form['title']->renderLabel() ?></th>
        <td>
          <?php echo $form['title']->renderError() ?>
          <?php echo $form['title'] ?>
        </td>
      </tr>
      <tr>
          <th><?php echo $form['title_de']->renderLabel() ?></th>
          <td>
            <?php echo $form['title_de']->renderError() ?>
            <?php echo $form['title_de'] ?>
          </td>
      </tr>    
	  	<tr>
          <th><?php echo $form['title_it']->renderLabel() ?></th>
          <td>
            <?php echo $form['title_it']->renderError() ?>
            <?php echo $form['title_it'] ?>
          </td>
      </tr>
	  	<tr>
          <th><?php echo $form['title_ko']->renderLabel() ?></th>
          <td>
            <?php echo $form['title_ko']->renderError() ?>
            <?php echo $form['title_ko'] ?>
          </td>
      </tr>
      <tr>
        <th><?php echo $form['intro']->renderLabel() ?></th>
        <td>
          <?php echo $form['intro']->renderError() ?>
          <?php echo $form['intro'] ?>
        </td>
      </tr>
      <tr>
          <th><?php echo $form['intro_de']->renderLabel() ?></th>
          <td>
            <?php echo $form['intro_de']->renderError() ?>
            <?php echo $form['intro_de'] ?>
          </td>
      </tr>    
	  	<tr>
          <th><?php echo $form['intro_it']->renderLabel() ?></th>
          <td>
            <?php echo $form['intro_it']->renderError() ?>
            <?php echo $form['intro_it'] ?>
          </td>
      </tr>
	  	<tr>
          <th><?php echo $form['intro_ko']->renderLabel() ?></th>
          <td>
            <?php echo $form['intro_ko']->renderError() ?>
            <?php echo $form['intro_ko'] ?>
          </td>
      </tr>
      <tr>          
        <th><?php echo $form['overview']->renderLabel() ?></th>
        <td>
          <?php echo $form['overview']->renderError() ?>
          <?php echo $form['overview'] ?>
        </td>
      </tr>
      <tr>          
        <th><?php echo $form['overview_de']->renderLabel() ?></th>
        <td>
          <?php echo $form['overview_de']->renderError() ?>
          <?php echo $form['overview_de'] ?>
        </td>
      </tr>
	 	 <tr>          
        <th><?php echo $form['overview_it']->renderLabel() ?></th>
        <td>
          <?php echo $form['overview_it']->renderError() ?>
          <?php echo $form['overview_it'] ?>
        </td>
      </tr>
	  	<tr>          
        <th><?php echo $form['overview_ko']->renderLabel() ?></th>
        <td>
          <?php echo $form['overview_ko']->renderError() ?>
          <?php echo $form['overview_ko'] ?>
        </td>
      </tr>
      <tr>          
        <th><?php echo $form['highlights']->renderLabel() ?></th>
        <td>
          <?php echo $form['highlights']->renderError() ?>
          <?php echo $form['highlights'] ?>
        </td>
      </tr>
      <tr>          
        <th><?php echo $form['highlights_de']->renderLabel() ?></th>
        <td>
          <?php echo $form['highlights_de']->renderError() ?>
          <?php echo $form['highlights_de'] ?>
        </td>
      </tr>
	 	 <tr>          
        <th><?php echo $form['highlights_it']->renderLabel() ?></th>
        <td>
          <?php echo $form['highlights_it']->renderError() ?>
          <?php echo $form['highlights_it'] ?>
        </td>
      </tr>
	  	<tr>          
        <th><?php echo $form['highlights_ko']->renderLabel() ?></th>
        <td>
          <?php echo $form['highlights_ko']->renderError() ?>
          <?php echo $form['highlights_ko'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['thumb']->renderLabel() ?></th>
        <td>
          <?php echo $form['thumb']->renderError() ?>
          <?php echo $form['thumb'] ?>
          <?php echo $form['thumb']->renderHelp() ?>
        </td>
      </tr>
      
      <tr>          
        <th><?php echo $form['cover']->renderLabel() ?></th>
        <td>
          <?php echo $form['cover']->renderError() ?>
          <?php echo $form['cover'] ?>
		  		<?php echo $form['cover']->renderHelp() ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['map']->renderLabel() ?></th>
        <td>
          <?php echo $form['map']->renderError() ?>
          <?php echo $form['map'] ?>
		  		<?php echo $form['map']->renderHelp() ?>
        </td>
      </tr>
      <tr>
          <th><?php echo $form['youtube']->renderLabel() ?></th>
          <td>
            <?php echo $form['youtube']->renderError() ?>
            <?php echo $form['youtube'] ?>
            <?php echo $form['youtube']->renderHelp() ?>
          </td>
      </tr>
      <tr>
          <th><?php echo $form['nb_days']->renderLabel() ?></th>
          <td>
            <?php echo $form['nb_days']->renderError() ?>
            <?php echo $form['nb_days'] ?>
            <?php echo $form['nb_days']->renderHelp() ?>
          </td>
      </tr>
	  	<tr>
          <th><?php echo $form['guests']->renderLabel() ?></th>
          <td>
            <?php echo $form['guests']->renderError() ?>
            <?php echo $form['guests'] ?>
            <?php echo $form['guests']->renderHelp() ?>
          </td>
      </tr>
      <tr>
          <th><?php echo $form['ages']->renderLabel() ?></th>
          <td>
            <?php echo $form['ages']->renderError() ?>
            <?php echo $form['ages'] ?>
            <?php echo $form['ages']->renderHelp() ?>
          </td>
      </tr>      
      <tr>
          <th><?php echo $form['price_from']->renderLabel() ?></th>
          <td>
            <?php echo $form['price_from']->renderError() ?>
            <?php echo $form['price_from'] ?>
            <?php echo $form['price_from']->renderHelp() ?>
          </td>
      </tr>
      <tr>
        <th><?php echo $form['meal']->renderLabel() ?></th>
        <td>
          <?php echo $form['meal']->renderError() ?>
          <?php echo $form['meal'] ?>
		  		<?php echo $form['meal']->renderHelp() ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['transport']->renderLabel() ?></th>
        <td>
          <?php echo $form['transport']->renderError() ?>
          <?php echo $form['transport'] ?>
		  		<?php echo $form['transport']->renderHelp() ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['accommodation']->renderLabel() ?></th>
        <td>
          <?php echo $form['accommodation']->renderError() ?>
          <?php echo $form['accommodation'] ?>
		  		<?php echo $form['accommodation']->renderHelp() ?>
        </td>
      </tr>
      <tr>
          <th><?php echo $form['included_activities']->renderLabel() ?></th>
          <td>
            <?php echo $form['included_activities']->renderError() ?>
            <?php echo $form['included_activities'] ?>
            <?php echo $form['included_activities']->renderHelp() ?>
          </td>
      </tr>
      <tr>
          <th><?php echo $form['important_notes']->renderLabel() ?></th>
          <td>
            <?php echo $form['important_notes']->renderError() ?>
            <?php echo $form['important_notes'] ?>
            <?php echo $form['important_notes']->renderHelp() ?>
          </td>
      </tr>
      <tr>
          <th><?php echo $form['latitude']->renderLabel() ?></th>
          <td>
            <?php echo $form['latitude']->renderError() ?>
            <?php echo $form['latitude'] ?>
            <?php echo $form['latitude']->renderHelp() ?>
          </td>
      </tr>
      <tr>
          <th><?php echo $form['longitude']->renderLabel() ?></th>
          <td>
            <?php echo $form['longitude']->renderError() ?>
            <?php echo $form['longitude'] ?>
            <?php echo $form['longitude']->renderHelp() ?>
          </td>
      </tr>
	  	<tr>
          <th><?php echo $form['sort']->renderLabel() ?></th>
          <td>
            <?php echo $form['sort']->renderError() ?>
            <?php echo $form['sort'] ?>
          </td>
      </tr>
      <tr>
          <th><?php echo $form['is_active']->renderLabel() ?></th>
          <td>
            <?php echo $form['is_active']->renderError() ?>
            <?php echo $form['is_active'] ?>
          </td>
      </tr>
	  	<tr>
          <th><?php echo $form['is_featured']->renderLabel() ?></th>
          <td>
            <?php echo $form['is_featured']->renderError() ?>
            <?php echo $form['is_featured'] ?>
          </td>
      </tr>
    </tbody>
  </table>
</form>

<script type="text/javascript">
</script>
<?php

/**
 * offer actions.
 *
 * @package    zzz
 * @subpackage offer
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class offerActions extends sfActions
{

  public function executePerm() {
      $this->getUser()->setFlash('flash', 'Permission denied!', true);
  }

  public function executeIndex(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('offer'), 'offer', 'perm');
      $params = array();
      $params['isActive'] = 'all';
      if($request->getParameter('s')) $params['s'] = $request->getParameter('s');
      $this->pager = GlobalTable::getPager('Offer', '*', $params);
  }

  public function executeNew(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('offer'), 'offer', 'perm');
      $this->form = new OfferForm();
      $this->setTemplate('edit');
  }
  
  public function executeCreate(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('offer'), 'offer', 'perm');
      $this->forward404Unless($request->isMethod(sfRequest::POST));
      $this->form = new OfferForm();
      $this->processForm($request, $this->form);
      $this->setTemplate('edit');
  }

  public function executeEdit(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('offer'), 'offer', 'perm');
      $this->forward404Unless($offer = Doctrine::getTable('Offer')->find(array($request->getParameter('id'))), sprintf('Object offer does not exist (%s).', $request->getParameter('id')));
      $this->form = new OfferForm($offer);
  }

  public function executeUpdate(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('offer'), 'offer', 'perm');
      $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
      $this->forward404Unless($offer = Doctrine::getTable('Offer')->find(array($request->getParameter('id'))), sprintf('Object offer does not exist (%s).', $request->getParameter('id')));
      $this->form = new OfferForm($offer);
      $this->processForm($request, $this->form);
      $this->setTemplate('edit');
  }
  
  public function executeDelete(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('offer'), 'offer', 'perm');
      $this->forward404Unless($offer = Doctrine::getTable('Offer')->find(array($request->getParameter('id'))), sprintf('Object offer does not exist (%s).', $request->getParameter('id')));
      try {
          $offer->delete();
          $this->getUser()->setFlash('flash', 'Successfully deleted.', true);
      } catch (Exception  $e){}
      $this->redirect('offer/index');
  }

  public function executeActivate(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('offer'), 'offer', 'perm');
      
      $this->forward404Unless($offer = Doctrine::getTable('Offer')->find($request->getParameter('id')));
      $this->forward404Unless(in_array($cmd = $request->getParameter('cmd'), array(0,1)));
      $offer->setIsActive($cmd);
      $offer->save();
      $this->getUser()->setFlash('flash', 'Successfully saved.', true);
      $this->redirect('offer/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form) {
      $this->forwardUnless($this->getUser()->hasCredential('offer'), 'offer', 'perm');
      $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
      if ($form->isValid()) {
		  $isNew = $form->isNew();
          $offer = $form->save();
          if(!$form->getValue('title_de')) $offer->setTitleDe($offer->getTitle());
          if(!$form->getValue('title_it')) $offer->setTitleIt($offer->getTitle());
          if(!$form->getValue('title_ko')) $offer->setTitleKo($offer->getTitle());
          if(!$form->getValue('intro_de')) $offer->setIntroDe($offer->getIntro());
          if(!$form->getValue('intro_it')) $offer->setIntroIt($offer->getIntro());
          if(!$form->getValue('intro_ko')) $offer->setIntroKo($offer->getIntro());
          if(!$form->getValue('overview_de')) $offer->setOverviewDe($offer->getOverview());
          if(!$form->getValue('overview_it')) $offer->setOverviewIt($offer->getOverview());
          if(!$form->getValue('overview_ko')) $offer->setOverviewKo($offer->getOverview());
          if(!$form->getValue('highlights_de')) $offer->setHighlightsDe($offer->getHighlights());
          if(!$form->getValue('highlights_it')) $offer->setHighlightsIt($offer->getHighlights());
          if(!$form->getValue('highlights_ko')) $offer->setHighlightsKo($offer->getHighlights());
          
		  		$offer->setRoute(GlobalLib::slugify($offer->getTitle()));
          $offer->setUpdatedAt(date('Y-m-d H:i:s'));
          if($isNew) $offer->setCreatedAid($this->getUser()->getId());
          $offer->setUpdatedAid($this->getUser()->getId());
          $offer->save();

          $this->getUser()->setFlash('flash', 'Successfully saved.', true);
          $this->redirect('offer/index?s='.$request->getParameter('s'));
      }
  }


}
<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<form action="<?php echo url_for('page/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          <?php echo $form->renderHiddenFields(false) ?>
          &nbsp;<a href="<?php echo url_for('page/index') ?>">Back to list</a>
          <input type="submit" value="Save" class="button" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
       <tr>
        <th><?php echo $form['title']->renderLabel() ?></th>
        <td>
          <?php echo $form['title']->renderError() ?>
          <?php echo $form['title'] ?>
        </td>
      </tr>
      <tr>
          <th><?php echo $form['title_de']->renderLabel() ?></th>
          <td>
            <?php echo $form['title_de']->renderError() ?>
            <?php echo $form['title_de'] ?>
          </td>
      </tr>     
	  <tr>
          <th><?php echo $form['title_it']->renderLabel() ?></th>
          <td>
            <?php echo $form['title_it']->renderError() ?>
            <?php echo $form['title_it'] ?>
          </td>
      </tr>  
	  <tr>
          <th><?php echo $form['title_ko']->renderLabel() ?></th>
          <td>
            <?php echo $form['title_ko']->renderError() ?>
            <?php echo $form['title_ko'] ?>
          </td>
      </tr>
	  <tr>          
        <th><?php echo $form['content']->renderLabel() ?></th>
        <td>
          <?php echo $form['content']->renderError() ?>
          <?php echo $form['content'] ?>
        </td>
      </tr>
      <tr>          
        <th><?php echo $form['content_de']->renderLabel() ?></th>
        <td>
          <?php echo $form['content_de']->renderError() ?>
          <?php echo $form['content_de'] ?>
        </td>
      </tr>
	  <tr>          
        <th><?php echo $form['content_it']->renderLabel() ?></th>
        <td>
          <?php echo $form['content_it']->renderError() ?>
          <?php echo $form['content_it'] ?>
        </td>
      </tr>
	  <tr>          
        <th><?php echo $form['content_ko']->renderLabel() ?></th>
        <td>
          <?php echo $form['content_ko']->renderError() ?>
          <?php echo $form['content_ko'] ?>
        </td>
      </tr>
	  <tr>
          <th><?php echo $form['sort']->renderLabel() ?></th>
          <td>
            <?php echo $form['sort']->renderError() ?>
            <?php echo $form['sort'] ?>
          </td>
      </tr>
	  <tr>
          <th><?php echo $form['is_active']->renderLabel() ?></th>
          <td>
            <?php echo $form['is_active']->renderError() ?>
            <?php echo $form['is_active'] ?>
          </td>
      </tr>
    </tbody>
  </table>
</form>

<script type="text/javascript">
$(document).ready(function(){
});
</script>
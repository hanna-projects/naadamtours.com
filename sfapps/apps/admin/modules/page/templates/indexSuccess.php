<form action="<?php echo url_for('page/index')?>" method="GET">
    <?php include_partial('partial/search', array());?>
</form>
<br clear="all">
<br clear="all">
<table width="100%">
  <thead>
    <tr>
      <th>#</th>
      <th>Manage</th>
      <th>Type</th>      
      <th>Title</th>      
      <th>De</th>
      <th>It</th>
      <th>Ko</th>
      <th>Status</th>
      <th>Sort</th>
      <th>Date</th>
      <th>Admin</th>
    </tr>
  </thead>
  <tbody>
		<?php $i=0; foreach ($pager->getResults() as $rs): ?>
		<tr style="background:<?php if(!$rs->getIsActive()) echo '#dedede;'?>">
		  <td><?php echo ++$i?></td>
		  <td nowrap>
			  <?php include_partial('partial/edit', array('module'=>'page', 'id'=>$rs->getId()));?>
			  <?php include_partial('partial/isActive', array('module'=>'page', 'rs'=>$rs));?>
		  </td>
		  <td><?php echo $rs->getType() ?></td>
		  <td><a href="<?php echo url_for('page/edit?id='.$rs->getId())?>"><?php echo $rs->getTitle() ?></a></td>
		  <td><?php echo $rs->getTitleDe() ?></td>
		  <td><?php echo $rs->getTitleIt() ?></td>
		  <td><?php echo $rs->getTitleKo() ?></td>
		  <?php include_partial('partial/td_active_featured', array('rs'=>$rs));?>
		  <?php include_partial('partial/td_sort_date_admin', array('rs'=>$rs));?>		  
		</tr>
		<?php endforeach; ?>
  </tbody>
</table>
<br clear="all">
<?php echo pager($pager, 'page/index?s='.$sf_params->get('s'))?>
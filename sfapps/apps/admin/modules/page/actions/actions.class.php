<?php

/**
 * page actions.
 *
 * @package    zzz
 * @subpackage page
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class pageActions extends sfActions
{

  public function executePerm() {
      $this->getUser()->setFlash('flash', 'Permission denied!', true);
  }

  public function executeIndex(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('page'), 'page', 'perm');
      $params = array();
      $params['isActive'] = 'all';
      if($request->getParameter('s')) $params['s'] = $request->getParameter('s');
      $this->pager = GlobalTable::getPager('Page', '*', $params);
  }

  public function executeEdit(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('page'), 'page', 'perm');
      $this->forward404Unless($page = Doctrine::getTable('Page')->find(array($request->getParameter('id'))), sprintf('Object page does not exist (%s).', $request->getParameter('id')));
      $this->form = new PageForm($page);
  }

  public function executeUpdate(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('page'), 'page', 'perm');
      $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
      $this->forward404Unless($page = Doctrine::getTable('Page')->find(array($request->getParameter('id'))), sprintf('Object page does not exist (%s).', $request->getParameter('id')));
      $this->form = new PageForm($page);
      $this->processForm($request, $this->form);
      $this->setTemplate('edit');
  }
  
  public function executeActivate(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('page'), 'page', 'perm');
      
      $this->forward404Unless($page = Doctrine::getTable('Page')->find($request->getParameter('id')));
      $this->forward404Unless(in_array($cmd = $request->getParameter('cmd'), array(0,1)));
      $page->setIsActive($cmd);
      $page->save();
      $this->getUser()->setFlash('flash', 'Successfully saved.', true);
      $this->redirect('page/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form) {
      $this->forwardUnless($this->getUser()->hasCredential('page'), 'page', 'perm');
      $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
      if ($form->isValid()) {
		  $isNew = $form->isNew();
          $page = $form->save();
          if(!$form->getValue('title_de')) $page->setTitleDe($page->getTitle());
          if(!$form->getValue('title_it')) $page->setTitleIt($page->getTitle());
          if(!$form->getValue('title_ko')) $page->setTitleKo($page->getTitle());
          if(!$form->getValue('content_de')) $page->setContentDe($page->getContent());
          if(!$form->getValue('content_it')) $page->setContentIt($page->getContent());
          if(!$form->getValue('content_ko')) $page->setContentKo($page->getContent());
          
		  		$page->setRoute(GlobalLib::slugify($page->getTitle()));
          $page->setUpdatedAt(date('Y-m-d H:i:s'));
          if($isNew) $page->setCreatedAid($this->getUser()->getId());
          $page->setUpdatedAid($this->getUser()->getId());
          $page->save();
    
          $this->getUser()->setFlash('flash', 'Successfully saved.', true);
          $this->redirect('page/index');
      }
  }


}
<?php

/**
 * dest actions.
 *
 * @package    zzz
 * @subpackage dest
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class destActions extends sfActions
{

  public function executePerm() {
      $this->getUser()->setFlash('flash', 'Permission denied!', true);
  }

  public function executeIndex(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('dest'), 'dest', 'perm');
      $params = array();
      $params['isActive'] = 'all';
      if($request->getParameter('s')) $params['s'] = $request->getParameter('s');
      $this->pager = GlobalTable::getPager('Destination', '*', $params);
  }

  public function executeNew(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('dest'), 'dest', 'perm');
      $this->form = new DestinationForm();
      $this->setTemplate('edit');
  }
  
  public function executeCreate(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('dest'), 'dest', 'perm');
      $this->forward404Unless($request->isMethod(sfRequest::POST));
      $this->form = new DestinationForm();
      $this->processForm($request, $this->form);
      $this->setTemplate('edit');
  }

  public function executeEdit(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('dest'), 'dest', 'perm');
      $this->forward404Unless($dest = Doctrine::getTable('Destination')->find(array($request->getParameter('id'))), sprintf('Object dest does not exist (%s).', $request->getParameter('id')));
      $this->form = new DestinationForm($dest);
  }

  public function executeUpdate(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('dest'), 'dest', 'perm');
      $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
      $this->forward404Unless($dest = Doctrine::getTable('Destination')->find(array($request->getParameter('id'))), sprintf('Object dest does not exist (%s).', $request->getParameter('id')));
      $this->form = new DestinationForm($dest);
      $this->processForm($request, $this->form);
      $this->setTemplate('edit');
  }
  
  public function executeDelete(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('dest'), 'dest', 'perm');
      $this->forward404Unless($dest = Doctrine::getTable('Destination')->find(array($request->getParameter('id'))), sprintf('Object dest does not exist (%s).', $request->getParameter('id')));
      try {
          $dest->delete();
          $this->getUser()->setFlash('flash', 'Successfully deleted.', true);
      } catch (Exception  $e){}
      $this->redirect('dest/index');
  }

  public function executeActivate(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('dest'), 'dest', 'perm');
      
      $this->forward404Unless($dest = Doctrine::getTable('Destination')->find($request->getParameter('id')));
      $this->forward404Unless(in_array($cmd = $request->getParameter('cmd'), array(0,1)));
      $dest->setIsActive($cmd);
      $dest->save();
      $this->getUser()->setFlash('flash', 'Successfully saved.', true);
      $this->redirect('dest/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form) {
      $this->forwardUnless($this->getUser()->hasCredential('dest'), 'dest', 'perm');
      $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
      if ($form->isValid()) {
		  $isNew = $form->isNew();
          $dest = $form->save();
          if(!$form->getValue('title_de')) $dest->setTitleDe($dest->getTitle());
          if(!$form->getValue('title_it')) $dest->setTitleIt($dest->getTitle());
          if(!$form->getValue('title_ko')) $dest->setTitleKo($dest->getTitle());
          if(!$form->getValue('intro_de')) $dest->setIntroDe($dest->getIntro());
          if(!$form->getValue('intro_it')) $dest->setIntroIt($dest->getIntro());
          if(!$form->getValue('intro_ko')) $dest->setIntroKo($dest->getIntro());
          if(!$form->getValue('overview_de')) $dest->setOverviewDe($dest->getOverview());
          if(!$form->getValue('overview_it')) $dest->setOverviewIt($dest->getOverview());
          if(!$form->getValue('overview_ko')) $dest->setOverviewKo($dest->getOverview());
          
          $dest->setOfferIds(join(";", $request->getParameter('suggested-offers')));
		  		$dest->setRoute(GlobalLib::slugify($dest->getTitle()));
          $dest->setUpdatedAt(date('Y-m-d H:i:s'));
          if($isNew) $dest->setCreatedAid($this->getUser()->getId());
          $dest->setUpdatedAid($this->getUser()->getId());
          $dest->save();

          $this->getUser()->setFlash('flash', 'Successfully saved.', true);
          $this->redirect('dest/index?s='.$request->getParameter('s'));
      }
  }


}
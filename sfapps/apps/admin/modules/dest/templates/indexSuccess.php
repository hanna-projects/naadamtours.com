<form action="<?php echo url_for('dest/index')?>" method="GET">
  <?php include_partial('partial/search', array());?>
</form>
<br clear="all">
<br clear="all">
<table width="100%">
  <thead>
    <tr>
      <th>#</th>
      <th>Manage</th>
      <th>Title</th>
      <th>De</th>
      <th>It</th>
      <th>Ko</th>
      <th>Thumb</th>
      <th>Cover</th>
      <th>Map</th>
      <th>Intro</th>
      <th>Intro De</th>
      <th>Intro It</th>
      <th>Intro Ko</th>
      <th>Status</th>
      <th>Sort</th>
      <th>Date</th>
      <th>Admin</th>
    </tr>
  </thead>
  <tbody>
		<?php $i=0; foreach ($pager->getResults() as $rs): ?>
		<tr style="background:<?php if(!$rs->getIsActive()) echo '#dedede;'?>">
		  <td><?php echo ++$i?></td>
		  <td nowrap>
				  <?php include_partial('partial/editDelete', array('module'=>'dest', 'id'=>$rs->getId()));?><br>
				  <?php include_partial('partial/isActive', array('module'=>'dest', 'rs'=>$rs));?>
				  <a href="<?php echo url_for('image/new?objectType=dest&objectId='.$rs->getId())?>" title="Images" class="action">Images</a>				  
		  </td>
		  <td><a href="<?php echo url_for('dest/edit?id='.$rs->getId())?>"><?php echo $rs->getTitle() ?></a></td>
		  <td><?php echo $rs->getTitleDe() ?></td>
		  <td><?php echo $rs->getTitleIt() ?></td>
		  <td><?php echo $rs->getTitleKo() ?></td>
		  <td><?php if($rs->getThumb()) echo image_tag('/u/dest/'.$rs->getThumb(), array('style'=>'max-width:200px;')) ?></td>
		  <td><?php if($rs->getCover()) echo image_tag('/u/dest/'.$rs->getCover(), array('style'=>'max-width:200px;')) ?></td>
		  <td><?php if($rs->getMap()) echo image_tag('/u/dest/'.$rs->getMap(), array('style'=>'max-width:200px;')) ?></td>
		  <td><?php echo $rs->getIntro() ?></td>
		  <td><?php echo $rs->getIntroDe() ?></td>
		  <td><?php echo $rs->getIntroIt() ?></td>
		  <td><?php echo $rs->getIntroKo() ?></td>
		  <?php include_partial('partial/td_active_featured', array('rs'=>$rs));?>
		  <?php include_partial('partial/td_sort_date_admin', array('rs'=>$rs));?>		  
		</tr>
		<?php endforeach; ?>
  </tbody>
</table>
<br clear="all">
<?php echo pager($pager, 'dest/index?s='.$sf_params->get('s'))?>
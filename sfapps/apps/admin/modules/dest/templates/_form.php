<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<?php $object = $form->getObject() ?>
<form action="<?php echo url_for('dest/'.($object->isNew() ? 'create' : 'update').(!$object->isNew() ? '?id='.$object->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
<?php if (!$object->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>

  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          <?php echo $form->renderHiddenFields(false) ?>
          &nbsp;<a href="<?php echo url_for('dest/index') ?>">Back to list</a>
          <input type="submit" value="Save" class="button" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><?php echo $form['title']->renderLabel() ?></th>
        <td>
          <?php echo $form['title']->renderError() ?>
          <?php echo $form['title'] ?>
        </td>
      </tr>
      <tr>
          <th><?php echo $form['title_de']->renderLabel() ?></th>
          <td>
            <?php echo $form['title_de']->renderError() ?>
            <?php echo $form['title_de'] ?>
          </td>
      </tr>    
	  	<tr>
          <th><?php echo $form['title_it']->renderLabel() ?></th>
          <td>
            <?php echo $form['title_it']->renderError() ?>
            <?php echo $form['title_it'] ?>
          </td>
      </tr>
	  	<tr>
          <th><?php echo $form['title_ko']->renderLabel() ?></th>
          <td>
            <?php echo $form['title_ko']->renderError() ?>
            <?php echo $form['title_ko'] ?>
          </td>
      </tr>
      <tr>
        <th><?php echo $form['intro']->renderLabel() ?></th>
        <td>
          <?php echo $form['intro']->renderError() ?>
          <?php echo $form['intro'] ?>
        </td>
      </tr>
      <tr>
          <th><?php echo $form['intro_de']->renderLabel() ?></th>
          <td>
            <?php echo $form['intro_de']->renderError() ?>
            <?php echo $form['intro_de'] ?>
          </td>
      </tr>    
	  	<tr>
          <th><?php echo $form['intro_it']->renderLabel() ?></th>
          <td>
            <?php echo $form['intro_it']->renderError() ?>
            <?php echo $form['intro_it'] ?>
          </td>
      </tr>
	  	<tr>
          <th><?php echo $form['intro_ko']->renderLabel() ?></th>
          <td>
            <?php echo $form['intro_ko']->renderError() ?>
            <?php echo $form['intro_ko'] ?>
          </td>
      </tr>
      <tr>          
        <th><?php echo $form['overview']->renderLabel() ?></th>
        <td>
          <?php echo $form['overview']->renderError() ?>
          <?php echo $form['overview'] ?>
        </td>
      </tr>
      <tr>          
        <th><?php echo $form['overview_de']->renderLabel() ?></th>
        <td>
          <?php echo $form['overview_de']->renderError() ?>
          <?php echo $form['overview_de'] ?>
        </td>
      </tr>
	 	 <tr>          
        <th><?php echo $form['overview_it']->renderLabel() ?></th>
        <td>
          <?php echo $form['overview_it']->renderError() ?>
          <?php echo $form['overview_it'] ?>
        </td>
      </tr>
	  	<tr>          
        <th><?php echo $form['overview_ko']->renderLabel() ?></th>
        <td>
          <?php echo $form['overview_ko']->renderError() ?>
          <?php echo $form['overview_ko'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['thumb']->renderLabel() ?></th>
        <td>
          <?php echo $form['thumb']->renderError() ?>
          <?php echo $form['thumb'] ?>
          <?php echo $form['thumb']->renderHelp() ?>
        </td>
      </tr>
      
      <tr>          
        <th><?php echo $form['cover']->renderLabel() ?></th>
        <td>
          <?php echo $form['cover']->renderError() ?>
          <?php echo $form['cover'] ?>
		  		<?php echo $form['cover']->renderHelp() ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['map']->renderLabel() ?></th>
        <td>
          <?php echo $form['map']->renderError() ?>
          <?php echo $form['map'] ?>
		  		<?php echo $form['map']->renderHelp() ?>
        </td>
      </tr>
      <tr>
          <th><?php echo $form['youtube']->renderLabel() ?></th>
          <td>
            <?php echo $form['youtube']->renderError() ?>
            <?php echo $form['youtube'] ?>
            <?php echo $form['youtube']->renderHelp() ?>
          </td>
      </tr>
      <tr>
          <th><?php echo $form['latitude']->renderLabel() ?></th>
          <td>
            <?php echo $form['latitude']->renderError() ?>
            <?php echo $form['latitude'] ?>
            <?php echo $form['latitude']->renderHelp() ?>
          </td>
      </tr>
      <tr>
          <th><?php echo $form['longitude']->renderLabel() ?></th>
          <td>
            <?php echo $form['longitude']->renderError() ?>
            <?php echo $form['longitude'] ?>
            <?php echo $form['longitude']->renderHelp() ?>
          </td>
      </tr>
      <tr>
		      <th>Suggested offers</th>
		      <td>
		        <div style="height:250px;overflow-y:scroll;">
		            <?php 
		            $ids = $object->getOfferIds() ? explode(";", $object->getOfferIds()) : array();
		            $rss = GlobalTable::doFetchSelection('Offer', 'title', 'id, title');		            
		            foreach ($rss as $k=>$v){
		                echo "<label><input type='checkbox' value='{$k}' name='suggested-offers[]' ".(in_array($k, $ids) ? 'checked' : '')."/>{$v}</label><br clear='all'>";
		            }
		            ?>
		        </div>
		      </td>
		  </tr>
	  	<tr>
          <th><?php echo $form['sort']->renderLabel() ?></th>
          <td>
            <?php echo $form['sort']->renderError() ?>
            <?php echo $form['sort'] ?>
          </td>
      </tr>
      <tr>
          <th><?php echo $form['is_active']->renderLabel() ?></th>
          <td>
            <?php echo $form['is_active']->renderError() ?>
            <?php echo $form['is_active'] ?>
          </td>
      </tr>
	  	<tr>
          <th><?php echo $form['is_featured']->renderLabel() ?></th>
          <td>
            <?php echo $form['is_featured']->renderError() ?>
            <?php echo $form['is_featured'] ?>
          </td>
      </tr>
    </tbody>
  </table>
</form>

<script type="text/javascript">
</script>
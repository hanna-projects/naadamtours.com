<form action="<?php echo url_for('slide/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>

<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          <?php echo $form->renderHiddenFields(false) ?>
          <input type="submit" value="Save" />
          &nbsp; <?php echo link_to('Back to list', 'slide/index', array()) ?>
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
          <th><?php echo $form['filename']->renderLabel() ?></th>
          <td>
            <?php echo $form['filename']->renderError() ?>
            <?php echo $form['filename'] ?>
          </td>
      </tr>
      <tr>
          <th><?php echo $form['link']->renderLabel() ?></th>
          <td>
            <?php echo $form['link']->renderError() ?>
            <?php echo $form['link'] ?>
          </td>
      </tr>      
      <tr>
          <th><?php echo $form['title']->renderLabel() ?></th>
          <td>
            <?php echo $form['title']->renderError() ?>
            <?php echo $form['title'] ?>
          </td>
      </tr>
      <tr>
          <th><?php echo $form['title_de']->renderLabel() ?></th>
          <td>
            <?php echo $form['title_de']->renderError() ?>
            <?php echo $form['title_de'] ?>
          </td>
      </tr>
      <tr>
          <th><?php echo $form['title_it']->renderLabel() ?></th>
          <td>
            <?php echo $form['title_it']->renderError() ?>
            <?php echo $form['title_it'] ?>
          </td>
      </tr>
      <tr>
          <th><?php echo $form['title_ko']->renderLabel() ?></th>
          <td>
            <?php echo $form['title_ko']->renderError() ?>
            <?php echo $form['title_ko'] ?>
          </td>
      </tr>
      <tr>
          <th><?php echo $form['description']->renderLabel() ?></th>
          <td>
            <?php echo $form['description']->renderError() ?>
            <?php echo $form['description'] ?>
          </td>
      </tr>
      <tr>
          <th><?php echo $form['description_de']->renderLabel() ?></th>
          <td>
            <?php echo $form['description_de']->renderError() ?>
            <?php echo $form['description_de'] ?>
          </td>
      </tr>
      <tr>
          <th><?php echo $form['description_it']->renderLabel() ?></th>
          <td>
            <?php echo $form['description_it']->renderError() ?>
            <?php echo $form['description_it'] ?>
          </td>
      </tr>
      <tr>
          <th><?php echo $form['description_ko']->renderLabel() ?></th>
          <td>
            <?php echo $form['description_ko']->renderError() ?>
            <?php echo $form['description_ko'] ?>
          </td>
      </tr>
      <tr>
          <th><?php echo $form['sort']->renderLabel() ?></th>
          <td>
            <?php echo $form['sort']->renderError() ?>
            <?php echo $form['sort'] ?>
          </td>
      </tr>
      <tr>
          <th><?php echo $form['is_active']->renderLabel() ?></th>
          <td>
            <?php echo $form['is_active']->renderError() ?>
            <?php echo $form['is_active'] ?>
          </td>
      </tr>
	  	<tr>
          <th><?php echo $form['is_featured']->renderLabel() ?></th>
          <td>
            <?php echo $form['is_featured']->renderError() ?>
            <?php echo $form['is_featured'] ?>
          </td>
      </tr>
    </tbody>
  </table>
</form>
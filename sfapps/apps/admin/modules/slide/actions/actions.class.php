<?php

/**
 * slide actions.
 *
 * @package    zzz
 * @subpackage slide
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class slideActions extends sfActions
{

  public function executePerm() {
      $this->getUser()->setFlash('flash', 'Permission denied!', true);
  }

  public function executeIndex(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('slide'), 'slide', 'perm');
      $params = array();
      $params['isActive'] = 'all';
      if($request->getParameter('s')) $params['s'] = $request->getParameter('s');
      $this->pager = GlobalTable::getPager('Slide', '*', $params);
  }

  public function executeNew(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('slide'), 'slide', 'perm');
      $this->form = new SlideForm();
      $this->setTemplate('edit');
  }
  
  public function executeCreate(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('slide'), 'slide', 'perm');
      $this->forward404Unless($request->isMethod(sfRequest::POST));
      $this->form = new SlideForm();
      $this->processForm($request, $this->form);
      $this->setTemplate('edit');
  }

  public function executeEdit(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('slide'), 'slide', 'perm');
      $this->forward404Unless($slide = Doctrine::getTable('Slide')->find(array($request->getParameter('id'))), sprintf('Object slide does not exist (%s).', $request->getParameter('id')));
      $this->form = new SlideForm($slide);
  }

  public function executeUpdate(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('slide'), 'slide', 'perm');
      $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
      $this->forward404Unless($slide = Doctrine::getTable('Slide')->find(array($request->getParameter('id'))), sprintf('Object slide does not exist (%s).', $request->getParameter('id')));
      $this->form = new SlideForm($slide);
      $this->processForm($request, $this->form);
      $this->setTemplate('edit');
  }
  
  public function executeDelete(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('slide'), 'slide', 'perm');
      $this->forward404Unless($slide = Doctrine::getTable('Slide')->find(array($request->getParameter('id'))), sprintf('Object slide does not exist (%s).', $request->getParameter('id')));
      try {
          $slide->delete();
          $this->getUser()->setFlash('flash', 'Successfully deleted.', true);
      } catch (Exception  $e){}
      $this->redirect('slide/index');
  }

  public function executeActivate(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('slide'), 'slide', 'perm');
      
      $this->forward404Unless($slide = Doctrine::getTable('Slide')->find($request->getParameter('id')));
      $this->forward404Unless(in_array($cmd = $request->getParameter('cmd'), array(0,1)));
      $slide->setIsActive($cmd);
      $slide->save();
      $this->getUser()->setFlash('flash', 'Successfully saved.', true);
      $this->redirect('slide/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form) {
      $this->forwardUnless($this->getUser()->hasCredential('slide'), 'slide', 'perm');
      $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
      if ($form->isValid()) {
          $slide = $form->save();
          if(!$form->getValue('title_de')) $slide->setTitleDe($slide->getTitle());
          if(!$form->getValue('title_it')) $slide->setTitleIt($slide->getTitle());
          if(!$form->getValue('title_ko')) $slide->setTitleKo($slide->getTitle());
          if(!$form->getValue('description_de')) $slide->setDescriptionDe($slide->getDescription());
          if(!$form->getValue('description_it')) $slide->setDescriptionIt($slide->getDescription());
          if(!$form->getValue('description_ko')) $slide->setDescriptionKo($slide->getDescription());

          $slide->setUpdatedAt(date('Y-m-d H:i:s'));
          $slide->save();
    
          $this->getUser()->setFlash('flash', 'Successfully saved.', true);
          $this->redirect('slide/index');
      }
  }


}
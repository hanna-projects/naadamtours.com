<?php

/**
 * testimonial actions.
 *
 * @package    zzz
 * @subpackage testimonial
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class testimonialActions extends sfActions
{

  public function executePerm() {
      $this->getUser()->setFlash('flash', 'Permission denied!', true);
  }

  public function executeIndex(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('testimonial'), 'testimonial', 'perm');
      $params = array();
      $params['isActive'] = 'all';
      if($request->getParameter('s')) $params['s'] = $request->getParameter('s');
      $this->pager = GlobalTable::getPager('Testimonial', '*', $params);
  }

  public function executeNew(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('testimonial'), 'testimonial', 'perm');
      $this->form = new TestimonialForm();
      $this->setTemplate('edit');
  }
  
  public function executeCreate(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('testimonial'), 'testimonial', 'perm');
      $this->forward404Unless($request->isMethod(sfRequest::POST));
      $this->form = new TestimonialForm();
      $this->processForm($request, $this->form);
      $this->setTemplate('edit');
  }

  public function executeEdit(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('testimonial'), 'testimonial', 'perm');
      $this->forward404Unless($testimonial = Doctrine::getTable('Testimonial')->find(array($request->getParameter('id'))), sprintf('Object testimonial does not exist (%s).', $request->getParameter('id')));
      $this->form = new TestimonialForm($testimonial);
  }

  public function executeUpdate(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('testimonial'), 'testimonial', 'perm');
      $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
      $this->forward404Unless($testimonial = Doctrine::getTable('Testimonial')->find(array($request->getParameter('id'))), sprintf('Object testimonial does not exist (%s).', $request->getParameter('id')));
      $this->form = new TestimonialForm($testimonial);
      $this->processForm($request, $this->form);
      $this->setTemplate('edit');
  }
  
  public function executeDelete(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('testimonial'), 'testimonial', 'perm');
      $this->forward404Unless($testimonial = Doctrine::getTable('Testimonial')->find(array($request->getParameter('id'))), sprintf('Object testimonial does not exist (%s).', $request->getParameter('id')));
      try {
          $testimonial->delete();
          $this->getUser()->setFlash('flash', 'Successfully deleted.', true);
      } catch (Exception  $e){}
      $this->redirect('testimonial/index');
  }

  public function executeActivate(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('testimonial'), 'testimonial', 'perm');
      
      $this->forward404Unless($testimonial = Doctrine::getTable('Testimonial')->find($request->getParameter('id')));
      $this->forward404Unless(in_array($cmd = $request->getParameter('cmd'), array(0,1)));
      $testimonial->setIsActive($cmd);
      $testimonial->save();
      $this->getUser()->setFlash('flash', 'Successfully saved.', true);
      $this->redirect('testimonial/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form) {
      $this->forwardUnless($this->getUser()->hasCredential('testimonial'), 'testimonial', 'perm');
      $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
      if ($form->isValid()) {
          $testimonial = $form->save();
          if(!$form->getValue('text_de')) $testimonial->setTextDe($testimonial->getText());
          if(!$form->getValue('text_it')) $testimonial->setTextIt($testimonial->getText());
          if(!$form->getValue('text_ko')) $testimonial->setTextKo($testimonial->getText());
          
          $testimonial->setUpdatedAt(date('Y-m-d H:i:s'));
          $testimonial->save();
    
          $this->getUser()->setFlash('flash', 'Successfully saved.', true);
          $this->redirect('testimonial/index');
      }
  }


}
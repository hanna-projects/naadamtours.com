<form action="<?php echo url_for('testimonial/index')?>" method="GET">
    <?php include_partial('partial/search', array());?>
</form>
<br clear="all">
<br clear="all">
<table width="100%">
  <thead>
    <tr>
      <th>#</th>
      <th>Manage</th>
      <th>Text</th>      
      <th>Text De</th>      
      <th>Text It</th>
      <th>Text Ko</th>
      <th>Fullname</th>
      <th>Position</th>
      <th>Signature</th>
      <th>Date</th>
      <th>Status</th>
      <th>Sort</th>
      <th>Date</th>
      <th>Admin</th>
    </tr>
  </thead>
  <tbody>
		<?php $i=0; foreach ($pager->getResults() as $rs): ?>
		<tr style="background:<?php if(!$rs->getIsActive()) echo '#dedede;'?>">
		  <td><?php echo ++$i?></td>
		  <td nowrap>
			  <?php include_partial('partial/editDelete', array('module'=>'testimonial', 'id'=>$rs->getId()));?>
			  <?php include_partial('partial/isActive', array('module'=>'testimonial', 'rs'=>$rs));?>
		  </td>
		  <td><div style="width:200px;"><a href="<?php echo url_for('testimonial/edit?id='.$rs->getId())?>"><?php echo $rs->getText() ?></a></div></td>
		  <td><div style="width:200px;"><?php echo $rs->getTextDe() ?></div></td>
		  <td><div style="width:200px;"><?php echo $rs->getTextIt() ?></div></td>
		  <td><div style="width:200px;"><?php echo $rs->getTextKo() ?></div></td>
		  <td><?php echo $rs->getFullname() ?></td>
		  <td><?php echo $rs->getPosition() ?></td>
		  <td><?php echo image_tag('/u/testimonial/'.$rs->getSignature(), array('style'=>'max-width:300px;'));?></td>
		  <td><?php echo $rs->getDate() ?></td>
		  <?php include_partial('partial/td_active_featured', array('rs'=>$rs));?>
		  <?php include_partial('partial/td_sort_date_admin', array('rs'=>$rs));?>		  
		</tr>
		<?php endforeach; ?>
  </tbody>
</table>
<br clear="all">
<?php echo pager($pager, 'testimonial/index?s='.$sf_params->get('s'))?>

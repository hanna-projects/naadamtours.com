<form action="<?php echo url_for('testimonial/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>

<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          <?php echo $form->renderHiddenFields(false) ?>
          <input type="submit" value="Save" />
          &nbsp; <?php echo link_to('Back to list', 'testimonial/index', array()) ?>
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
          <th><?php echo $form['text']->renderLabel() ?></th>
          <td>
            <?php echo $form['text']->renderError() ?>
            <?php echo $form['text'] ?>
          </td>
      </tr>
      <tr>
          <th><?php echo $form['text_de']->renderLabel() ?></th>
          <td>
            <?php echo $form['text_de']->renderError() ?>
            <?php echo $form['text_de'] ?>
          </td>
      </tr>      
      <tr>
          <th><?php echo $form['text_it']->renderLabel() ?></th>
          <td>
            <?php echo $form['text_it']->renderError() ?>
            <?php echo $form['text_it'] ?>
          </td>
      </tr>
      <tr>
          <th><?php echo $form['text_ko']->renderLabel() ?></th>
          <td>
            <?php echo $form['text_ko']->renderError() ?>
            <?php echo $form['text_ko'] ?>
          </td>
      </tr>
      <tr>
          <th><?php echo $form['fullname']->renderLabel() ?></th>
          <td>
            <?php echo $form['fullname']->renderError() ?>
            <?php echo $form['fullname'] ?>
          </td>
      </tr>
      <tr>
          <th><?php echo $form['position']->renderLabel() ?></th>
          <td>
            <?php echo $form['position']->renderError() ?>
            <?php echo $form['position'] ?>
          </td>
      </tr>
      <tr>
          <th><?php echo $form['date']->renderLabel() ?></th>
          <td>
            <?php echo $form['date']->renderError() ?>
            <?php echo $form['date'] ?>
          </td>
      </tr>
      <tr>
          <th><?php echo $form['signature']->renderLabel() ?></th>
          <td>
            <?php echo $form['signature']->renderError() ?>
            <?php echo $form['signature'] ?>
          </td>
      </tr>
      <tr>
          <th><?php echo $form['sort']->renderLabel() ?></th>
          <td>
            <?php echo $form['sort']->renderError() ?>
            <?php echo $form['sort'] ?>
          </td>
      </tr>
      <tr>
          <th><?php echo $form['is_active']->renderLabel() ?></th>
          <td>
            <?php echo $form['is_active']->renderError() ?>
            <?php echo $form['is_active'] ?>
          </td>
      </tr>
	  	<tr>
          <th><?php echo $form['is_featured']->renderLabel() ?></th>
          <td>
            <?php echo $form['is_featured']->renderError() ?>
            <?php echo $form['is_featured'] ?>
          </td>
      </tr>
    </tbody>
  </table>
</form>
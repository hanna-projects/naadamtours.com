<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<?php $object = $form->getObject() ?>
<form action="<?php echo url_for('item/'.($object->isNew() ? 'create' : 'update').(!$object->isNew() ? '?id='.$object->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
<?php if (!$object->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>

  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          <?php echo $form->renderHiddenFields(false) ?>
          &nbsp;<a href="<?php echo url_for('item/index') ?>">Back to list</a>
          <input type="submit" value="Save" class="button" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
	  <tr>
        <th><?php echo $form['page_id']->renderLabel() ?></th>
        <td>
          <?php echo $form['page_id']->renderError() ?>
          <?php echo $form['page_id'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['title']->renderLabel() ?></th>
        <td>
          <?php echo $form['title']->renderError() ?>
          <?php echo $form['title'] ?>
        </td>
      </tr>
      <tr>
          <th><?php echo $form['title_de']->renderLabel() ?></th>
          <td>
            <?php echo $form['title_de']->renderError() ?>
            <?php echo $form['title_de'] ?>
          </td>
      </tr>     
	  	<tr>
          <th><?php echo $form['title_it']->renderLabel() ?></th>
          <td>
            <?php echo $form['title_it']->renderError() ?>
            <?php echo $form['title_it'] ?>
          </td>
      </tr>  
	  	<tr>
          <th><?php echo $form['title_ko']->renderLabel() ?></th>
          <td>
            <?php echo $form['title_ko']->renderError() ?>
            <?php echo $form['title_ko'] ?>
          </td>
      </tr>
		  <tr>          
	        <th><?php echo $form['image1']->renderLabel() ?></th>
	        <td>
	          <?php echo $form['image1']->renderError() ?>
	          <?php echo $form['image1'] ?>
			  <?php echo $form['image1']->renderHelp() ?>
	        </td>
	      </tr>
		  <tr>          
        <th><?php echo $form['summary']->renderLabel() ?></th>
        <td>
          <?php echo $form['summary']->renderError() ?>
          <?php echo $form['summary'] ?>
        </td>
      </tr>
      <tr>          
        <th><?php echo $form['summary_de']->renderLabel() ?></th>
        <td>
          <?php echo $form['summary_de']->renderError() ?>
          <?php echo $form['summary_de'] ?>
        </td>
      </tr>
		  <tr>          
	        <th><?php echo $form['summary_it']->renderLabel() ?></th>
	        <td>
	          <?php echo $form['summary_it']->renderError() ?>
	          <?php echo $form['summary_it'] ?>
	        </td>
	      </tr>
		  <tr>          
        <th><?php echo $form['summary_ko']->renderLabel() ?></th>
        <td>
          <?php echo $form['summary_ko']->renderError() ?>
          <?php echo $form['summary_ko'] ?>
        </td>
      </tr>
	  	<tr>          
        <th><?php echo $form['content']->renderLabel() ?></th>
        <td>
          <?php echo $form['content']->renderError() ?>
          <?php echo $form['content'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['content_de']->renderLabel() ?></th>
        <td>
          <?php echo $form['content_de']->renderError() ?>
          <?php echo $form['content_de'] ?>
        </td>
      </tr>
	  	<tr>          
        <th><?php echo $form['content_it']->renderLabel() ?></th>
        <td>
          <?php echo $form['content_it']->renderError() ?>
          <?php echo $form['content_it'] ?>
        </td>
      </tr>
	  	<tr>          
        <th><?php echo $form['content_ko']->renderLabel() ?></th>
        <td>
          <?php echo $form['content_ko']->renderError() ?>
          <?php echo $form['content_ko'] ?>
        </td>
      </tr>
	  	<tr>
          <th><?php echo $form['sort']->renderLabel() ?></th>
          <td>
            <?php echo $form['sort']->renderError() ?>
            <?php echo $form['sort'] ?>
          </td>
      </tr>
      <tr>
          <th><?php echo $form['is_active']->renderLabel() ?></th>
          <td>
            <?php echo $form['is_active']->renderError() ?>
            <?php echo $form['is_active'] ?>
          </td>
      </tr>
	  	<tr>
          <th><?php echo $form['is_featured']->renderLabel() ?></th>
          <td>
            <?php echo $form['is_featured']->renderError() ?>
            <?php echo $form['is_featured'] ?>
          </td>
      </tr>
    </tbody>
  </table>
</form>
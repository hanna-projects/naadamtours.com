<form action="<?php echo url_for('item/index')?>" method="GET">
	<?php $pages = GlobalTable::doFetchSelection('Page', 'title', 'title', array());?>
    <b>Page</b>&nbsp;
    <select name="pageId" id="pageId">
    <?php foreach ($pages as $key=>$value){
        echo '<option value="'.$key.'" '.(($sf_params->get('pageId') == $key) ? 'selected' : '').'>'.$value.'</option>';
    }?>
    </select> &nbsp; 
    <?php include_partial('partial/search', array());?>
</form>
<br clear="all">
<br clear="all">
<table width="100%">
  <thead>
    <tr>
      <th>#</th>
      <th>Manage</th>
      <th>Page</th>
      <th>Title</th>      
      <th>De</th>
      <th>It</th>
      <th>Ko</th>
      <th>Main picture</th>
      <th>Status</th>
      <th>Sort</th>
      <th>Date</th>
      <th>Admin</th>
    </tr>
  </thead>
  <tbody>
		<?php $i=0; foreach ($pager->getResults() as $rs): ?>
		<tr style="background:<?php if(!$rs->getIsActive()) echo '#dedede;'?>">
		  <td><?php echo ++$i?></td>
		  <td nowrap>
			  <?php include_partial('partial/editDelete', array('module'=>'item', 'id'=>$rs->getId()));?><br>
			  <?php include_partial('partial/isActive', array('module'=>'item', 'rs'=>$rs));?>
			  <a href="<?php echo url_for('image/new?objectType=item&objectId='.$rs->getId())?>" title="Images" class="action">More pictures</a><br>
			  <a href="<?php echo url_for('subitem/index?itemId='.$rs->getId())?>" title="Subitems" class="action">Subitems</a>
		  </td>
		  <td><?php echo $rs->getPage()->getTitle() ?></td>
		  <td><a href="<?php echo url_for('item/edit?id='.$rs->getId())?>"><?php echo $rs->getTitle() ?></a></td>
		  <td><?php echo $rs->getTitleDe() ?></td>
		  <td><?php echo $rs->getTitleIt() ?></td>
		  <td><?php echo $rs->getTitleKo() ?></td>
		  <td><?php if($rs->getImage1()) echo image_tag('/u/item/'.$rs->getImage1(), array('style'=>'max-width:200px;')) ?></td>
		  <?php include_partial('partial/td_active_featured', array('rs'=>$rs));?>
		  <?php include_partial('partial/td_sort_date_admin', array('rs'=>$rs));?>		  
		</tr>
		<?php endforeach; ?>
  </tbody>
</table>
<br clear="all">
<?php echo pager($pager, 'item/index?s='.$sf_params->get('s').'&pageId='.$sf_params->get('pageId'))?>

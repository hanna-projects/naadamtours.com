<?php

/**
 * item actions.
 *
 * @package    zzz
 * @subpackage item
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class itemActions extends sfActions
{

  public function executePerm() {
      $this->getUser()->setFlash('flash', 'Permission denied!', true);
  }

  public function executeIndex(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('item'), 'item', 'perm');
      $params = array();
      $params['isActive'] = 'all';
      if($request->getParameter('s')) $params['s'] = $request->getParameter('s');
      if($request->getParameter('pageId')) $params['pageId'] = $request->getParameter('pageId');
      $this->pager = GlobalTable::getPager('Item', '*', $params);
  }

  public function executeNew(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('item'), 'item', 'perm');
      $this->form = new ItemForm();
      $this->setTemplate('edit');
  }
  
  public function executeCreate(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('item'), 'item', 'perm');
      $this->forward404Unless($request->isMethod(sfRequest::POST));
      $this->form = new ItemForm();
      $this->processForm($request, $this->form);
      $this->setTemplate('edit');
  }

  public function executeEdit(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('item'), 'item', 'perm');
      $this->forward404Unless($item = Doctrine::getTable('Item')->find(array($request->getParameter('id'))), sprintf('Object item does not exist (%s).', $request->getParameter('id')));
      $this->form = new ItemForm($item);
  }

  public function executeUpdate(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('item'), 'item', 'perm');
      $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
      $this->forward404Unless($item = Doctrine::getTable('Item')->find(array($request->getParameter('id'))), sprintf('Object item does not exist (%s).', $request->getParameter('id')));
      $this->form = new ItemForm($item);
      $this->processForm($request, $this->form);
      $this->setTemplate('edit');
  }
  
  public function executeDelete(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('item'), 'item', 'perm');
      $this->forward404Unless($item = Doctrine::getTable('Item')->find(array($request->getParameter('id'))), sprintf('Object item does not exist (%s).', $request->getParameter('id')));
      try {
          $item->delete();
          $this->getUser()->setFlash('flash', 'Successfully deleted.', true);
      } catch (Exception  $e){}
      $this->redirect('item/index');
  }

  public function executeActivate(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('item'), 'item', 'perm');
      
      $this->forward404Unless($item = Doctrine::getTable('Item')->find($request->getParameter('id')));
      $this->forward404Unless(in_array($cmd = $request->getParameter('cmd'), array(0,1)));
      $item->setIsActive($cmd);
      $item->save();
      $this->getUser()->setFlash('flash', 'Successfully saved.', true);
      $this->redirect('item/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form) {
      $this->forwardUnless($this->getUser()->hasCredential('item'), 'item', 'perm');
      $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
      if ($form->isValid()) {
		  $isNew = $form->isNew();
          $item = $form->save();
		  		$item->setRoute(GlobalLib::slugify($item->getTitle()));
          $item->setUpdatedAt(date('Y-m-d H:i:s'));
          if(!$form->getValue('title_de')) $item->setTitleDe($item->getTitle());
          if(!$form->getValue('title_it')) $item->setTitleIt($item->getTitle());
          if(!$form->getValue('title_ko')) $item->setTitleKo($item->getTitle());
          if(!$form->getValue('summary_de')) $item->setSummaryDe($item->getSummary());
          if(!$form->getValue('summary_it')) $item->setSummaryIt($item->getSummary());
          if(!$form->getValue('summary_ko')) $item->setSummaryKo($item->getSummary());
          if(!$form->getValue('content_de')) $item->setContentDe($item->getContent());
          if(!$form->getValue('content_it')) $item->setContentIt($item->getContent());
          if(!$form->getValue('content_ko')) $item->setContentKo($item->getContent());
          
          if($isNew) $item->setCreatedAid($this->getUser()->getId());
          $item->setUpdatedAid($this->getUser()->getId());
          $item->save();
          
          if($item->getIsFeatured() && !file_exists(sfConfig::get('sf_upload_dir').'/item/300t-'.$item->getImage1())) 
          		GlobalLib::createThumbs($item->getImage1(), 'item', array(300));

          $this->getUser()->setFlash('flash', 'Successfully saved.', true);
          $this->redirect('item/index?pageId='.$item->getPageId());
      }
  }


}
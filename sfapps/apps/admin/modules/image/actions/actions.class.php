<?php

/**
 * image actions.
 *
 * @package    
 * @subpackage image
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class imageActions extends sfActions
{

  public function preExecute() {
      $this->forwardUnless($this->getUser()->hasCredential('image'), 'admin', 'perm');
  }
  
  public function executeNew(sfWebRequest $request)
  {
  		$this->objectId = $request->getParameter('objectId');
  		$this->objectType = $request->getParameter('objectType');
      $this->form = new ImageForm(null, array('objectId'=>$this->objectId, 'objectType'=>$this->objectType));
  }

  public function executeCreate(sfWebRequest $request)
  {
      $this->forward404Unless($request->isMethod(sfRequest::POST));
      $form = $request->getParameter('image');
      $this->objectId = $request->getParameter('objectId') ? $request->getParameter('objectId') : $form['object_id'];
      $this->objectType = $request->getParameter('objectType') ? $request->getParameter('objectType') : $form['object_type'];
      
      $this->form = new ImageForm(null, array('objectId'=>$this->objectId, 'objectType'=>$this->objectType));
      $this->processForm($request, $this->form);
      $this->setTemplate('new');
  }
  

  public function executeEdit(sfWebRequest $request)
  {
      $this->forward404Unless($image = Doctrine_Core::getTable('Image')->find(array($request->getParameter('id'))), sprintf('Object image does not exist (%s).', $request->getParameter('id')));
      $this->objectId = $image->getObjectId();
  		$this->objectType = $image->getObjectType();
      $this->form = new ImageForm($image, array('objectId'=>$image->getObjectId(), 'objectType'=>$image->getObjectType()));
  }
  
  public function executeUpdate(sfWebRequest $request)
  {
      $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
      $this->forward404Unless($image = Doctrine_Core::getTable('Image')->find(array($request->getParameter('id'))), sprintf('Object image does not exist (%s).', $request->getParameter('id')));
      $this->objectId = $image->getObjectId();
  		$this->objectType = $image->getObjectType();
      $this->form = new ImageForm($image, array('objectId'=>$image->getObjectId(), 'objectType'=>$image->getObjectType()));
      $this->processForm($request, $this->form);
      $this->setTemplate('edit');
  }
  
  public function executeDelete(sfWebRequest $request)
  {
      $this->forward404Unless($image = Doctrine_Core::getTable('Image')->find(array($request->getParameter('id'))), sprintf('Object image does not exist (%s).', $request->getParameter('id')));
      $objectId = $image->getObjectId();
      $objectType = $image->getObjectType();
      $image->delete();
      $this->getUser()->setFlash('flash', 'Successfully deleted.', true);
      $this->redirect('image/new?objectType='.$objectType.'&objectId='.$objectId);
  }


  protected function processForm(sfWebRequest $request, sfForm $form)
  {
      $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
      if ($form->isValid()) {
		  	  $isNew = $form->isNew();
          $image = $form->save();
          if(!$form->getValue('title_de')) $image->setTitleDe($image->getTitle());
          if(!$form->getValue('title_it')) $image->setTitleIt($image->getTitle());
          if(!$form->getValue('title_ko')) $image->setTitleKo($image->getTitle());
          if(!$form->getValue('description_de')) $image->setDescriptionDe($image->getDescription());
          if(!$form->getValue('description_it')) $image->setDescriptionIt($image->getDescription());
          if(!$form->getValue('description_ko')) $image->setDescriptionKo($image->getDescription());
          $image->setUpdatedAt(date('Y-m-d H:i:s'));
          if($isNew) $image->setCreatedAid($this->getUser()->getId());
          $image->setUpdatedAid($this->getUser()->getId());
          $image->save();
          
          if($image->getIsFeatured() && !file_exists(sfConfig::get('sf_upload_dir').'/images/200t-'.$image->getFilename())) 
          		GlobalLib::createThumbs($image->getFilename(), 'images', array(200));

          $this->getUser()->setFlash('flash', 'Successfully saved.', true);
          $this->redirect('image/new?objectType='.$image->getObjectType().'&objectId='.$image->getObjectId());
      }
  }

}
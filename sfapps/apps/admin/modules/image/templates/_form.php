<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<form action="<?php echo url_for('image/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '?objectId='.$objectId.'&objectType='.$objectType)) ?>" method="POST" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
<?php if (!$form->getObject()->isNew()): ?>
  <input type="hidden" name="sf_method" value="put" />
<?php endif; ?>

<table width="40%" style="float:left;margin:0 10px 0 0;">
  <tfoot>
    <tr><td colspan="2" style="padding:5px;">&nbsp;</td></tr>
    <tr>
      <td colspan="2">
        <?php echo $form->renderHiddenFields(false) ?>
        <input type="submit" value="Save" />
      </td>
    </tr>
  </tfoot>
  <tbody>
    <?php echo $form->renderGlobalErrors() ?>
    <tr>
        <td valign="top">
            <div style="margin:10px 0 1px 0;font-weight:bold;">File *:</div>
            <?php echo $form['filename']->renderError() ?>
            <?php echo $form['filename'] ?>
            <?php echo $form['filename']->renderHelp() ?>
			
						<div style="margin:10px 0 1px 0;font-weight:bold;">Title:</div>
            <?php echo $form['title']->renderError() ?>
            <?php echo $form['title'] ?>
            <?php echo $form['title']->renderHelp() ?>
			
						<div style="margin:10px 0 1px 0;font-weight:bold;">Title de:</div>
            <?php echo $form['title_de']->renderError() ?>
            <?php echo $form['title_de'] ?>
            <?php echo $form['title_de']->renderHelp() ?>
			
						<div style="margin:10px 0 1px 0;font-weight:bold;">Title it:</div>
            <?php echo $form['title_it']->renderError() ?>
            <?php echo $form['title_it'] ?>
            <?php echo $form['title_it']->renderHelp() ?>
			
							<div style="margin:10px 0 1px 0;font-weight:bold;">Title ko:</div>
            <?php echo $form['title_ko']->renderError() ?>
            <?php echo $form['title_ko'] ?>
            <?php echo $form['title_ko']->renderHelp() ?>
          
            <div style="margin:10px 0 1px 0;font-weight:bold;">Desc:</div>
            <?php echo $form['description']->renderError() ?>
            <?php echo $form['description'] ?>
            <?php echo $form['description']->renderHelp() ?>
						<div style="margin:10px 0 1px 0;font-weight:bold;">Desc de:</div>
            <?php echo $form['description_de']->renderError() ?>
            <?php echo $form['description_de'] ?>
            <?php echo $form['description_de']->renderHelp() ?>
						<div style="margin:10px 0 1px 0;font-weight:bold;">Desc it:</div>
            <?php echo $form['description_it']->renderError() ?>
            <?php echo $form['description_it'] ?>
            <?php echo $form['description_it']->renderHelp() ?>
						<div style="margin:10px 0 1px 0;font-weight:bold;">Desc ko:</div>
            <?php echo $form['description_ko']->renderError() ?>
            <?php echo $form['description_ko'] ?>
            <?php echo $form['description_ko']->renderHelp() ?>
       
            <br clear="all">
            <label style="display:block;margin:10px 0 1px 0;font-weight:bold;">
                <?php echo $form['is_active']->renderError() ?>
                <?php echo $form['is_active'] ?> Is active	
                <?php echo $form['is_active']->renderHelp() ?>
            </label>
			
						<br clear="all">
            <label style="display:block;margin:10px 0 1px 0;font-weight:bold;">
                <?php echo $form['is_featured']->renderError() ?>
                <?php echo $form['is_featured'] ?> Is featured
                <?php echo $form['is_featured']->renderHelp() ?>
            </label>
            
            <br clear="all">
            <div style="margin:10px 0 1px 0;font-weight:bold;">Sort:</div>
            <?php echo $form['sort']->renderError() ?>
            <?php echo $form['sort'] ?>
            <?php echo $form['sort']->renderHelp() ?>
        </td>
    </tr>
  </tbody>
</table>

<div style="width:500px;float:left;line-height:30px;">
    <?php $images = GlobalTable::doFetchArray('Image', 'id, filename, title, description', 
                  array('objectId'=>$objectId, 'objectType'=>$objectType, 'isActive'=>'all', 'orderBy'=>'sort ASC', 'limit'=>100));?>
    <?php foreach ($images as $image) {?>
    		<b><?php echo GlobalLib::clearOutput($image['title']);?></b><br>
        <?php echo image_tag('/u/images/'.$image['filename'], array('style'=>'margin:0 0 5px 0;max-width:600px;'));?><br>

				<a href="<?php echo url_for('image/edit?id='.$image['id'])?>" title="Edit image">[edit image]</a> &nbsp; 
        <a onclick="return confirm('Are you sure?')" href="<?php echo url_for('image/delete?id='.$image['id'])?>" title="Delete">[delete image]</a><br>
        
        <?php echo GlobalLib::clearOutput($image['description']);?>
        <br clear="all">
        <hr style="border:0;border-bottom:1px dashed #dedede;margin:7px 0;">
    <?php }?>
</div>

</form>
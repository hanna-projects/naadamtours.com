<?php

/**
 * subitem actions.
 *
 * @package    
 * @subpackage subitem
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class subitemActions extends sfActions
{

  public function executePerm() {
      $this->getUser()->setFlash('flash', 'Permission denied!', true);
  }

  public function executeIndex(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('subitem'), 'subitem', 'perm');
      $params = array();
      $params['isActive'] = 'all';
      if($request->getParameter('s')) $params['s'] = $request->getParameter('s');
      if($request->getParameter('itemId')) $params['itemId'] = $request->getParameter('itemId');
      $this->pager = GlobalTable::getPager('Subitem', '*', $params);
  }
  
  
  public function executeNew(sfWebRequest $request)
  {
      $this->forward404Unless($this->item = Doctrine_Core::getTable('Item')->find($request->getParameter('itemId')));
      $this->form = new SubitemForm(null, array('itemId'=>$this->item->getId()));
  }

  public function executeCreate(sfWebRequest $request)
  {
      $this->forward404Unless($request->isMethod(sfRequest::POST));
      $form = $request->getParameter('subitem');
      $itemId = $request->getParameter('itemId') ? $request->getParameter('itemId') : $form['item_id'];
      $this->forward404Unless($this->item = Doctrine_Core::getTable('Item')->find($itemId));
      
      $this->form = new SubitemForm(null, array('itemId'=>$this->item->getId()));
      $this->processForm($request, $this->form);
      $this->setTemplate('new');
  }
  

  public function executeEdit(sfWebRequest $request)
  {
      $this->forward404Unless($subitem = Doctrine_Core::getTable('Subitem')->find(array($request->getParameter('id'))), sprintf('Object subitem does not exist (%s).', $request->getParameter('id')));
	  $this->forward404Unless($this->item = Doctrine_Core::getTable('Item')->find($subitem->getItemId()));
      $this->form = new SubitemForm($subitem, array('itemId'=>$subitem->getItemId()));
  }
  
  public function executeUpdate(sfWebRequest $request)
  {
      $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
      $this->forward404Unless($subitem = Doctrine_Core::getTable('Subitem')->find(array($request->getParameter('id'))), sprintf('Object subitem does not exist (%s).', $request->getParameter('id')));
	  $this->forward404Unless($this->item = Doctrine_Core::getTable('Item')->find($subitem->getItemId()));
      $this->form = new SubitemForm($subitem, array('itemId'=>$subitem->getItemId()));
      $this->processForm($request, $this->form);
      $this->setTemplate('edit');
  }
  
  public function executeDelete(sfWebRequest $request)
  {
      $this->forward404Unless($subitem = Doctrine_Core::getTable('Subitem')->find(array($request->getParameter('id'))), sprintf('Object subitem does not exist (%s).', $request->getParameter('id')));
      $itemId = $subitem->getItemId();
      $subitem->delete();
      $this->getUser()->setFlash('flash', 'Successfully deleted.', true);
      $this->redirect('subitem/new?itemId='.$itemId);
  }
  
  public function executeActivate(sfWebRequest $request) {
      $this->forwardUnless($this->getUser()->hasCredential('subitem'), 'subitem', 'perm');
      
      $this->forward404Unless($subitem = Doctrine::getTable('Subitem')->find($request->getParameter('id')));
      $this->forward404Unless(in_array($cmd = $request->getParameter('cmd'), array(0,1)));
      $subitem->setIsActive($cmd);
      $subitem->save();
      $this->getUser()->setFlash('flash', 'Successfully saved.', true);
      $this->redirect('subitem/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form) {
      $this->forwardUnless($this->getUser()->hasCredential('subitem'), 'subitem', 'perm');
      $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
      if ($form->isValid()) {
		  $isNew = $form->isNew();
          $subitem = $form->save();
          if(!$form->getValue('title_de')) $subitem->setTitleDe($subitem->getTitle());
          if(!$form->getValue('title_it')) $subitem->setTitleIt($subitem->getTitle());
          if(!$form->getValue('title_ko')) $subitem->setTitleKo($subitem->getTitle());
          if(!$form->getValue('content_de')) $subitem->setContentDe($subitem->getContent());
          if(!$form->getValue('content_it')) $subitem->setContentIt($subitem->getContent());
          if(!$form->getValue('content_ko')) $subitem->setContentKo($subitem->getContent());
          
		  		$subitem->setRoute(GlobalLib::slugify($subitem->getTitle()));
          $subitem->setUpdatedAt(date('Y-m-d H:i:s'));
          if($isNew) $subitem->setCreatedAid($this->getUser()->getId());
          $subitem->setUpdatedAid($this->getUser()->getId());
          $subitem->save();
    
          $this->getUser()->setFlash('flash', 'Successfully saved.', true);
          $this->redirect('subitem/index?itemId='.$subitem->getItemId());
      }
  }


}
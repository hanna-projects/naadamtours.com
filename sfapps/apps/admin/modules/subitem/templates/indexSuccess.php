<form action="<?php echo url_for('subitem/index')?>" method="GET">
	<?php $items = GlobalTable::doFetchSelection('Item', 'title', 'title', array());?>
    <b>Item</b>&nbsp;
    <select name="itemId" id="itemId">
    <?php foreach ($items as $key=>$value){
        echo '<option value="'.$key.'" '.(($sf_params->get('itemId') == $key) ? 'selected' : '').'>'.$value.'</option>';
    }?>
    </select> &nbsp; 
    <?php include_partial('partial/search', array());?>
</form>
<br clear="all">
<br clear="all">
<table width="100%">
  <thead>
    <tr>
      <th>#</th>
      <th>Manage</th>
      <th>Item</th>      
      <th>Title</th>      
      <th>De</th>
      <th>It</th>
      <th>Ko</th>
	  	<th>Image</th>
      <th>Status</th>
      <th>Sort</th>
      <th>Date</th>
      <th>Admin</th>
    </tr>
  </thead>
  <tbody>
		<?php $i=0; foreach ($pager->getResults() as $rs): ?>
		<tr style="background:<?php if(!$rs->getIsActive()) echo '#dedede;'?>">
		  <td><?php echo ++$i?></td>
		  <td nowrap>
			  <?php include_partial('partial/editDelete', array('module'=>'subitem', 'id'=>$rs->getId()));?>
			  <?php include_partial('partial/isActive', array('module'=>'subitem', 'rs'=>$rs));?>
		  </td>
		  <td><?php echo $rs->getItem()->getTitle() ?></td>
		  <td><a href="<?php echo url_for('subitem/edit?id='.$rs->getId().'&itemId='.$rs->getItemId())?>"><?php echo $rs->getTitle() ?></a></td>
		  <td><?php echo $rs->getTitleDe() ?></td>
		  <td><?php echo $rs->getTitleIt() ?></td>
		  <td><?php echo $rs->getTitleKo() ?></td>
		  <td><?php if($rs->getImage1()) echo image_tag('/u/subitem/'.$rs->getImage1(), array('style'=>'max-width:200px;')) ?></td>
		  <?php include_partial('partial/td_active_featured', array('rs'=>$rs));?>
		  <?php include_partial('partial/td_sort_date_admin', array('rs'=>$rs));?>		  
		</tr>
		<?php endforeach; ?>
  </tbody>
</table>
<br clear="all">
<?php echo pager($pager, 'subitem/index?s='.$sf_params->get('s').'&itemId='.$sf_params->get('itemId'))?>

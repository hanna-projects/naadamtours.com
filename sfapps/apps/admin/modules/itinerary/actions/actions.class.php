<?php

class itineraryActions extends sfActions
{

    public function preExecute() {
        $this->forwardUnless($this->getUser()->hasCredential('itinerary'), 'admin', 'perm');
    }

    public function executeIndex(sfWebRequest $request) 
    {
        $this->forward404Unless($offerId = $request->getParameter('offerId'));

        $params = [
            'isActive' =>'all',
            'offerId'  => $offerId,
            'orderBy'  => 'day ASC',
        ];
        if($request->getParameter('s')) $params['s'] = $request->getParameter('s');
        $this->pager = GlobalTable::getPager('Itinerary', '*', $params);

        $itinerary = null;
        if($request->getParameter('id')) {
            $this->forward404Unless($itinerary = Doctrine::getTable('Itinerary')->find(array($request->getParameter('id'))), sprintf('Object itinerary does not exist (%s).', $request->getParameter('id')));
        }
        $this->form = new ItineraryForm($itinerary, array('offerId'=>$offerId));
    }

    public function executeUpdate(sfWebRequest $request) 
    {
        $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));

        $itinerary = null;
        if($request->getParameter('id')) {
            $this->forward404Unless($itinerary = Doctrine::getTable('Itinerary')->find(array($request->getParameter('id'))), sprintf('Object itinerary does not exist (%s).', $request->getParameter('id')));
        }
        $this->form = new ItineraryForm($itinerary, array('offerId'=>($itinerary ? $itinerary->getOfferId() : $request->getParameter('offerId'))));
        $this->processForm($request, $this->form);
        $this->setTemplate('index');
    }
    
    public function executeDelete(sfWebRequest $request) 
    {
        $this->forward404Unless($itinerary = Doctrine::getTable('Itinerary')->find(array($request->getParameter('id'))), sprintf('Object itinerary does not exist (%s).', $request->getParameter('id')));

        $offerId = $itinerary->getOfferId();
        try {
            $itinerary->delete();
            $this->getUser()->setFlash('flash', 'Successfully deleted.', true);
        } catch (Exception  $e){}
        $this->redirect('itinerary/index?offerId='.$offerId);
    }

    protected function processForm(sfWebRequest $request, sfForm $form) 
    {
        $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
        if ($form->isValid()) {
            $isNew = $form->isNew();
            $itinerary = $form->save();
            if(!$form->getValue('title_de')) $itinerary->setTitleDe($itinerary->getTitle());
            if(!$form->getValue('title_it')) $itinerary->setTitleIt($itinerary->getTitle());
            if(!$form->getValue('title_ko')) $itinerary->setTitleKo($itinerary->getTitle());
            if(!$form->getValue('content_de')) $itinerary->setContentDe($itinerary->getContent());
            if(!$form->getValue('content_it')) $itinerary->setContentIt($itinerary->getContent());
            if(!$form->getValue('content_ko')) $itinerary->setContentKo($itinerary->getContent());

            $itinerary->setUpdatedAt(date('Y-m-d H:i:s'));
            if($isNew) $itinerary->setCreatedAid($this->getUser()->getId());
            $itinerary->setUpdatedAid($this->getUser()->getId());
            $itinerary->save();

            $this->getUser()->setFlash('flash', 'Successfully saved.', true);
            $this->redirect('itinerary/index?offerId='.$itinerary->getOfferId());
        }
    }


}
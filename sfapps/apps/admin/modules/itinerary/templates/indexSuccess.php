<form action="<?php echo url_for('itinerary/index?offerId='.$sf_params->get('offerId'))?>" method="GET">
  	<?php include_partial('partial/search', array());?>
</form>
<br clear="all">
<br clear="all">

<div class="left" style="min-width:450px;">
		<?php include_partial('form', array('form' => $form)) ?>
</div>

<div class="left" style="width:1145px;">
<table width="100%">
  <thead>
    <tr>
    	<th>Manage</th>
      <th>Day</th>
      <th>Title</th>
      <th>De</th>
      <th>It</th>
      <th>Ko</th>      
      <th>Image</th>
      <th>Meals</th>
    </tr>
  </thead>
  <tbody>
		<?php foreach ($pager->getResults() as $rs): ?>
		<tr style="background:<?php if(!$rs->getIsActive()) echo '#dedede;'?>">
			<td nowrap>
				<a href="<?php echo url_for('itinerary/index?id='.$rs->getId().'&offerId='.$rs->getOfferId())?>" title="Edit" class="action">Edit</a> | 
				<a onclick="return confirm('Are you sure?')" href="<?php echo url_for('itinerary/delete?id='.$rs->getId())?>" title="Delete" class="action">Delete</a>
		  </td>
		  <td><?php echo $rs->getDay()?></td>
		  <td><a href="<?php echo url_for('itinerary/index?id='.$rs->getId().'&offerId='.$rs->getOfferId())?>">
		  		<?php echo $rs->getTitle()?></a><br>
					<?php echo $rs->getContent() ?>
		  </td>
		  <td><b><?php echo $rs->getTitleDe() ?></b><br>
					<?php echo $rs->getContentDe() ?>
		  </td>
		  <td><b><?php echo $rs->getTitleIt()?></b><br>
					<?php echo $rs->getContentIt() ?>
		  </td>
		  <td><b><?php echo $rs->getTitleKo()?></b><br>
		  		<?php echo $rs->getContentKo()?>
			</td>		  
		  <td><?php if($rs->getImage()) echo image_tag('/u/itinerary/'.$rs->getImage(), array('style'=>'max-width:200px;')) ?></td>
		  <td><?php echo $rs->getMeals() ?></td>
		</tr>
		<?php endforeach; ?>
  </tbody>
</table>
<br clear="all">
<?php echo pager($pager, 'itinerary/index?offerId='.$sf_params->get('offerId'))?>
</div>
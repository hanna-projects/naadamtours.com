<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>  
  <?php include_http_metas() ?>
  <?php include_metas() ?>
  <?php include_title() ?>
	<?php include_stylesheets() ?>    
  <?php include_javascripts() ?>
	<?php $host = sfConfig::get('app_host')?>
	<link rel="shortcut icon" href="<?php echo $host?>/favicon.ico" />
  <!--jquery-->
  <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>-->
	<script src="<?php echo $host?>/js/jquery.min.js"></script>  
</head>

<body>
    <?php if($sf_user->isAuthenticated()):?>
        <div id="topmenu"><ul>
            <?php $tab = isset($tab) ? $tab : $sf_request->getParameter('module'); ?>
            <?php $act = isset($act) ? $act : $sf_request->getParameter('action'); ?>
            		<li <?php echo $tab == 'slide' ? 'class="current"' : '' ?>>
                  <?php echo link_to('Slide', 'slide/index')?>
                </li>
                <li <?php echo $tab == 'offer' ? 'class="current"' : '' ?>>
                  <?php echo link_to('Offer', 'offer/index')?>
                </li>
                <li <?php echo $tab == 'dest' ? 'class="current"' : '' ?>>
                  <?php echo link_to('Destination', 'dest/index')?>
                </li>
                <li <?php echo $tab == 'page' ? 'class="current"' : '' ?>>
                  <?php echo link_to('Page', 'page/index')?>
                </li>
								<li <?php echo $tab == 'item' ? 'class="current"' : '' ?>>
                  <?php echo link_to('Item', 'item/index')?>
                </li>
								<li <?php echo $tab == 'subitem' ? 'class="current"' : '' ?>>
                  <?php echo link_to('Subitem', 'subitem/index')?>
                </li>
                <li <?php echo $tab == 'testimonial' ? 'class="current"' : '' ?>>
                  <?php echo link_to('Testimonial', 'testimonial/index')?>
                </li>
								<li <?php echo $tab == 'feedback' ? 'class="current"' : '' ?>>
                  <?php echo link_to('Feedback', 'feedback/index')?>
                </li>
                <li <?php echo $tab == 'admin' ? 'class="current"' : '' ?>>
                    <?php echo link_to('Admin', 'admin/index')?>
                </li>
            <li><?php echo link_to('Logout ('.$sf_user->getEmail().')', 'admin/logout')?></li>
        </ul></div><!--topmenu-->
    
        <br clear="all">   
        <div id="submenu">
            <?php 
            if($tab == 'itinerary') { 
            		echo link_to('+ new', 'itinerary/index?offerId='.$sf_params->get('offerId'));
            } else if($tab == 'image') { 
            		echo link_to('+ new', 'image/new?objectId='.$sf_params->get('objectId').'&objectType='.$sf_params->get('objectType'));
            } else {
            		include_partial('partial/sublink', array('tab'=>$tab, 'hidden'=>array('page')));
            }?>
            <br clear="all">
        </div><!--topmenu-->
    <?php endif ?>

    <div id="wrapper">
      <div id="content" class="full">
          <?php if ($sf_user->hasFlash('flash')): ?>
              <div class="flash"><?php echo $sf_user->getFlash('flash')?></div>
          <?php endif; ?>
  
          <?php echo $sf_content ?>
      </div><!--content-->
    </div><!--wrapper-->
    
    <?php if($sf_user->isAuthenticated()):?>
        <div id="footer">
          2015 - 2017 &copy;  <a href="http://<?php echo sfConfig::get('app_webname')?>" target="_blank"><?php echo sfConfig::get('app_webname')?></a>
          <br clear="all">
        </div>
    <?php endif;?>
    
</body>
</html>

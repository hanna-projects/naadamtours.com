<?php

/**
 * main actions.
 *
 * @package    
 * @subpackage main
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 3335 2007-01-23 16:19:56Z fabien $
 */
class mainActions extends sfActions
{

    public function preExecute()
    {
        
    }
  
    public function executeHome(sfWebRequest $request)
    {
    }   
    
    public function execute404(sfWebRequest $request)
    {
        
    }

		public function executeCulture(sfWebRequest $request)
    {
        $culture = in_array($request->getParameter('l'), array('en','de','it','ko')) ? $request->getParameter('l') : 'en';
        $this->getUser()->setCulture($culture);
        $this->getResponse()->setCookie('culture', $culture);
        $this->redirect($request->getReferer() ? $request->getReferer() : '@homepage');
    }
    
    public function executeSearch(sfWebRequest $request)
    {
    		//TODO
        $this->pager = Doctrine::getTable('Offer')->getPager(
            array('keyword'=>$request->getParameter('keyword'), 'limit'=>24), $request->getParameter('page'));
        
    }
	
		public function executeSubmitEnquiry(sfWebRequest $request)
    {
				echo $errStr = "Unsuccessful, please try again a few minutes later."; die();
				if($request->isMethod(sfRequest::POST)) {
						$fullname = GlobalLib::clearInput($request->getParameter('fullname'));
						$email = GlobalLib::clearInput($request->getParameter('email')); 
						// TODO: validate email
						$message = GlobalLib::clearInput($request->getParameter('message'));
						if($fullname && $email && $message) {
								$feedback = new Feedback();
								$feedback->setFullname($fullname);
								$feedback->setEmail($email);
								$feedback->setMessage($message);
								$feedback->setIpAddress($request->getRemoteAddress());
								$feedback->save();
								// sending email
								$body = $this->getPartial("partial/mailFeedback", array('rs'=>$feedback));
								$message = $this->getMailer()->compose($feedback->getEmail(), sfConfig::get('app_feedbackmail'), 'porta.mn | feedback', $body);
								try {
									$this->getMailer()->send($message);
								} catch (Exception $e) {}
								// success
								$str = <<<EOF
										<script type="text/javascript">
											$('#feedback-error').html("Your feedback is successfully sent. Thank you for your time.");
											$('#feedback-error').css('color', 'green');
										</script>
EOF;
								return $this->renderText($str);
						}
						else $errStr = "Please fill the required fields.";
				}
				$str = <<<EOF
						<script type="text/javascript">
							$('#feedback-error').html("{$errStr}");
						</script>
EOF;
      	return $this->renderText($str);
    }
    

    public function executeTmp(sfWebRequest $request)
    {
        echo 'success'; die();
    }

}

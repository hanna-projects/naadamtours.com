<!--aboutus-->
<div class="page">
	<a id="aboutus" style="margin:0 0 70px 0;display:block;color:white;">aboutus</a>
	
	<?php $page = PageTable::doFetchOne('id, title, title_de, title_it, title_ko, content, content_de, content_it, content_ko, updated_at', array('type'=>'aboutus'))?>
	<?php include_partial("main/content_culture", array('page'=>$page));?>
	
	<!--items-->
	<?php $rss = ItemTable::doExecute('id, title, title_de, title_it, title_ko, summary, summary_de, summary_it, summary_ko, content, content_de, content_it, content_ko, updated_at', 
																		array('pageId'=>$page->getId(), 'limit'=>3))?>
	<?php $i = 0;?>
	<?php foreach($rss as $rs):?>
		<div class="left" style="width:270px;text-align:justify;margin:0 <?php echo ++$i >= 3 ? 0 : 70?>px 0 0;"> 
				<a class="fancybox" href="#fancybox-about-<?php echo $rs->getId()?>">
						<h2><?php echo $rs?></h2>
						<?php echo GlobalLib::clearOutput($rs->getSummaryCulture())?> ..
				</a>
		</div>
		<!--fancy hidden content-->
		<div id="fancybox-about-<?php echo $rs->getId()?>" class="fancy-custom">
				<h2 style="text-align:left;"><?php echo $rs?></h2>
				<div class="timestamp"><?php echo __('Last updated');?> <?php echo time_ago($rs->getUpdatedAt())?></div>
				<?php echo $rs->getContentCulture()?>
				<br clear="all">
				<br clear="all">
				
				<!--main image-->
				<?php if($rs->getImage1()):?>
					<?php echo image_tag('/u/item/'.$rs->getImage1(), array('style'=>'max-width:950px;border-radius:5px;'));?>
					<br clear="all">
				<?php endif?>
			
				<br clear="all">
				
				<!--images-->
				<?php $images = GlobalTable::doFetchArray('Image', 'id, filename, title, description', 
				              array('objectId'=>$rs->getId(), 'objectType'=>'item', 'isActive'=>'all', 'orderBy'=>'sort ASC', 'limit'=>100));?>
				<?php foreach ($images as $image) {?>				 
			    <?php echo image_tag('/u/images/'.$image['filename'], array('style'=>'max-width:950px;border-radius:5px;'));?>
			    <b><?php echo GlobalLib::clearOutput($image['title']);?></b><br>
					<?php echo GlobalLib::clearOutput($image['description']);?>
					<br clear="all">
			    <br clear="all">
				<?php }?>
		</div>
	<?php endforeach?>
	<br clear="all">
</div>

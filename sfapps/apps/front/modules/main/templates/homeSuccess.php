<?php include_partial("main/slide", array());?>

<?php include_partial("main/aboutus", array());?>
<?php include_partial("main/service", array());?>
<?php include_partial("main/offer", array());?>
<?php include_partial("main/destnation", array());?>
<?php include_partial("main/team", array());?>
<?php include_partial("main/testimonial", array());?>
<?php include_partial("main/travel_info", array());?>
<br clear="all">		
<?php include_partial("main/media", array());?>
<?php include_partial("main/contact", array());?>

<script type="text/javascript">
	$(document).ready(function() {
		$(".fancybox").fancybox({
			minWidth	: '90%',
			minHeight	: '90%',
			autoSize	: true,
			closeClick	: false,
			openEffect	: 'none',
			closeEffect	: 'none'
		});
	});
</script>
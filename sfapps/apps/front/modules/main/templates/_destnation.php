<!--destination-->
<div class="page">
	<a id="destination" style="margin:0 0 70px 0;display:block;color:white;">destination</a>
	
	<?php $page = PageTable::doFetchOne('id, title, title_de, title_it, title_ko, content, content_de, content_it, content_ko, updated_at', array('type'=>'destination'))?>
	<?php include_partial("main/content_culture", array('page'=>$page));?>
	
	<?php $rss = DestinationTable::doExecute('id, route, title, title_de, title_it, title_ko, thumb', array('limit'=>8))?>
	<?php $i = 0;?>
	<?php foreach($rss as $rs):?>
		<div class="left" style="width:210px;height:220px;padding:20px 0 0 0;text-align:left;margin:0 <?php echo ++$i == 4 ? 0 : 35?>px 0 0">
			<?php if($i == 4) $i = 0;?>
			<a href="<?php echo url_for('page/dest?route='.$rs->getRoute())?>">
				<?php echo image_tag('/u/dest/'.$rs->getThumb(), array('style'=>'width:210px;height:145px;border:1px solid #dedede;padding:2px;', 'class'=>'image', 'alt'=>$rs, 'title'=>$rs)) ?>
				<span class="right" style="margin:10px 2px 0 0;">
					<?php echo image_tag('icons/showmore.png', array('alt'=>__('Show more'), 'title'=>__('Show more'))) ?>
				</span>
				<h2 style="text-align:left;"><?php echo $rs?></h2>
			</a>
			<br clear="all">
		</div>
	<?php endforeach?>
	<br clear="all">
</div>


<!--page - yes-->
<!--dest - yes-->
<!--images - yes-->
<!--items - ?-->
<!--subitems - ?-->
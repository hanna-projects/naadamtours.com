<!--tour info-->
<div id="tour-info" class="page right" style="width:450px;padding:20px 0;">
	<?php $page = GlobalTable::doFetchOne('Page', array('content, content_de, content_it, content_ko, updated_at'), array('type'=>'travel-info'))?>
	<a class="fancybox" href="#fancybox-tour-info">
		<h3><?php echo $page?></h3>
		<div class="timestamp"><?php echo __('Last updated');?> <?php echo time_ago($page->getUpdatedAt())?></div>
		<?php echo utf8_substr($page->getContentCulture(), 0, 190)?> ..
	</a>	
	<!--fancy hidden content-->
	<div id="fancybox-tour-info" style="display:none">
		<h3><?php echo $page?></h3>
		<div class="timestamp"><?php echo __('Last updated');?> <?php echo time_ago($page->getUpdatedAt())?></div>
		<?php echo GlobalLib::clearOutput($page->getContentCulture())?>
	</div>
</div>
<br clear="all">
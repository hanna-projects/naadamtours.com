<form action="#" method="post" class="enquiry">
	<?php $form = new FeedbackForm()?>
	<?php echo $form->renderGlobalErrors() ?>

	<h4 style="text-align:center;">FIRSTLY, TELL US ABOUT YOURSELF</h4>
	<br clear="all">
	<?php echo $form['title'] ?>
	<?php echo $form['firstname'] ?>
	<?php echo $form['lastname'] ?>
	<br clear="all">
	<?php echo $form['email'] ?>
	<br clear="all">
	<?php echo $form['phone'] ?>
	<br clear="all">
	<?php echo $form['besttime2call'] ?>
	<br clear="all">
	<br clear="all">
		
  	<h4 style="text-align:center;cursor:pointer;" onclick="$('#secondly').slideToggle();">SECONDLY, TELL US ABOUT YOUR TRIP
		(THE MORE DETAIL, THE BETTER), SUCH AS… &raquo;</h4>
		<div id="secondly" style="display:none;">
			<div class="desc-gray">
				&#9642; Where do you wish to travel, and with whom?
				&#9642; Is there a particular itinerary on our website that you like the sound of?
				&#9642; What are your preferred dates of travel and trip length?
				&#9642; Estimated per person budget?</div>
			<?php echo $form['enquiry'] ?>
			<div class="desc-lightgray">
				&#9642; We will arrange the best possible itinerary based on your budget and advise where to save and where splurge<br>
				&#9642; We will advise you if your chosen time coincides with any festivals, events, big crowds or bad weather..
			</div>
			<br clear="all">
			
			<h4 style="text-align:center;">What type of accommodations do you prefer?</h4>
			<?php echo $form['accommodation'] ?>
			<br clear="all">
		</div>

		<br clear="all">
		<br clear="all">
		<input type="button" class="right" value="<?php echo __('Submit Enquiry')?>" onclick="submitEnquiry();" id="enquiry-submit"
			style="height:45px;padding:5px 20px;width:100%;text-align:center;background:#2f0343;color:#fff;text-transform:uppercase;" />
		<?php echo image_tag('icons/loading-blue.gif', array('id'=>'enquiry-loader', 'style'=>'display:none;', 'alt'=>__('loading'), 'title'=>__('loading')))?>

		<div class="alert alert-success" id="enquiry-success" style="display:none;">
			Your enquiry has been successfully sent. We are contacting you within next 2 business days. 
		</div>
</form>

<script src="<?php echo $host?>addons/phonemask/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?php echo $host?>addons/phonemask/jquery.inputmask.js" type="text/javascript"></script>
<script src="<?php echo $host?>addons/phonemask/jquery.bind-first-0.2.0.min.js" type="text/javascript"></script>
<script src="<?php echo $host?>addons/phonemask/jquery.inputmask-multi.js" type="text/javascript"></script>

<script type="text/javascript">

function submitEnquiry()
{
	if($("#enquiry_firstname").val().trim() == '') $("#enquiry_firstname").addClass('required');
	if($("#enquiry_lastname").val().trim() == '') $('#enquiry_lastname').addClass('required');
	if($("#enquiry_email").val().trim() == '') $('#enquiry_email').addClass('required');
	if($("#enquiry_phone").val().trim() == '') $('#enquiry_phone').addClass('required');
	if($("#enquiry_enquiry").val().trim() == '') $('#enquiry_enquiry').addClass('required');

	if($('#enquiry_firstname').hasClass('required') || $('#enquiry_lastname').hasClass('required') || $('#enquiry_email').hasClass('required')
		|| $('#enquiry_phone').hasClass('required'))
		return false;

	$.ajax({
		url: "<?php echo url_for('main/submitEnquiry')?>", 
		type: "POST",
		data: {firstname:$('#enquiry_firstname').val(), lastname:$('#enquiry_lastname').val(), email:$('#enquiry_email').val(), phone:$('#enquiry_phone').val(), besttime2call:$('#besttime2call').val(), 
						enquiry:$('#enquiry_enquiry').val(), motivation:$('#enquiry_motivation').val(), accommodation:$('#enquiry_accommodation').val()
		},
		beforeSend: function(){
			$('#enquiry-loader').show();
			$('#enquiry-submit').hide();
		},
		success: function(data) {
			$('#enquiry-loader').hide();
			$('#enquiry-success').slideDown();
		}
	});
}

$(document).ready(function() {

	$('#enquiry_firstname, #enquiry_lastname, #enquiry_email, #enquiry_phone').blur(function() {
		if($(this).val().trim() == '') $(this).addClass('required');
		else $(this).removeClass('required');
	});

	maskList = $.masksSort($.masksLoad("<?php echo $host?>addons/phonemask/phone-codes.json"), ["#"], /[0-9]|#/, "mask");
	var maskOpts = {
		inputmask: {
			definitions: {"#": {validator: "[0-9]", cardinality: 1}},
			showMaskOnHover: !1,
			autoUnmask: !1
		},
		match: /[0-9]/,
		replace: "#",
		list: maskList,
		listKey: "mask",
		onMaskChange: function(c, a) {}
	};

	$("#enquiry_phone").inputmasks(maskOpts);

});
</script>


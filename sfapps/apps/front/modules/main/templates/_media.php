<!--media-->
<div class="page">	
		<a id="media" style="margin:0 0 70px 0;display:block;color:white;">media</a>
		<?php $page = PageTable::doFetchOne('id, title, title_de, title_it, title_ko, content, content_de, content_it, content_ko, updated_at', array('type'=>'media'))?>
		<?php include_partial("main/content_culture", array('page'=>$page));?>
		
		<!--items-->
		<?php $rss = ItemTable::doExecute('id, route, title, title_de, title_it, title_ko, image1, summary, summary_de, summary_it, summary_ko, content, content_de, content_it, content_ko, updated_at', 
																			array('pageId'=>$page->getId(), 'limit'=>4, 'isFeatured'=>1))?>
		<?php $i = 0;?>
		<?php foreach($rss as $rs):?>
				<?php $id = $rs->getId()?>
				<a href="<?php echo url_for('page/blog').'#'.$rs->getRoute()?>" class="fancybox">
						<?php echo image_tag('/u/item/t300-'.$rs->getImage1(), array('style'=>'margin:5px 0 0 0;width:270px;height:160px;', 'class'=>'image left', 'alt'=>$rs, 'title'=>$rs)) ?>
				</a>
				<div class="right" style="width:640px;height:160px;margin:0 0 10px 0;position:relative;"> 
						<a href="<?php echo url_for('page/blog').'#'.$rs->getRoute()?>" class="fancybox">
								<h2 style="text-align:left;"><?php echo $rs?></h2>
								<div class="timestamp"><?php echo __('Last updated');?> <?php echo time_ago($rs->getUpdatedAt())?></div>
								<div style="text-align:justify;">
										<?php echo GlobalLib::clearOutput($rs->getSummaryCulture())?>	
								</div>
						</a>
						<a href="<?php echo url_for('page/blog').'#'.$rs->getRoute()?>" class="left showmore" style="position:absolute;bottom:-6px;">
								<?php echo __('Go To Blog')?> <?php echo image_tag('icons/arrow-right.png', array('alt'=>__('Go To Blog'), 'title'=>__('Go To Blog'))) ?></a>
						<br clear="all">
				</div>
				<br clear="all">			
				<br clear="all">			
		<?php endforeach?>
</div>
<br clear="all">

<!--page - yes-->
<!--items - yes-->
<!--subitems - no-->
<!--images - yes-->
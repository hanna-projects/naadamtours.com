<!--offer-->
<div class="page">
		<a id="offer" style="margin:0 0 70px 0;display:block;color:white;">offer</a>
		<?php $page = PageTable::doFetchOne('id, title, title_de, title_it, title_ko, content, content_de, content_it, content_ko, updated_at', array('type'=>'offer'))?>
		<?php include_partial("main/content_culture", array('page'=>$page));?>
			
		<!--items-->
		<?php $rss = OfferTable::doExecute('id, route, title, title_de, title_it, title_ko, thumb, intro, intro_de, intro_it, intro_ko, nb_days, price_from, guests', array('limit'=>4, 'isFeatured'=>1))?>
		<?php $i = 0;?>
		<?php foreach($rss as $rs):?>
				<div class="<?php echo ++$i%2 == 0 ? 'right' : 'left'?>" style="width:420px;height:280px;padding:20px 0 0 0;text-align:left;"> 
						<a href="<?php echo url_for('page/offer?route='.$rs->getRoute())?>">
								<?php echo image_tag('/u/offer/'.$rs->getThumb(), array('style'=>'width:420px;height:150px;margin-bottom:7px;', 'class'=>'image', 'alt'=>$rs, 'title'=>$rs)) ?>
								<h2 style="text-align:left;width:400px;"><?php echo $rs?></h2>
								<?php echo GlobalLib::clearOutput($rs->getIntroCulture())?>
								<div class="timestamp" style="font-size:12px;">
										<?php echo $rs->getNbDays().' '.__('days')?> 
										<?php if($rs->getPriceFrom()) echo __('from').' '.$rs->getPriceFrom().' per person'?>
								</div>
						</a>
						<a href="<?php echo url_for('page/offer?route='.$rs->getRoute())?>" class="right" style="margin:-23px 0 0 0;">
								<?php echo image_tag('icons/showmore.png', array('alt'=>__('Show more'), 'title'=>__('Show more'))) ?>
						</a>
						<br clear="all">
				</div>
		<?php endforeach?>
		<br clear="all">
</div>


<!--page - yes-->
<!--offer - yes-->
<!--itinerary - yes-->
<!--images - yes-->
<!--items - ?-->
<!--subitems - ?-->

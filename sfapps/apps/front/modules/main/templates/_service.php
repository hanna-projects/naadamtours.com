<!--service-->
<div class="page">
	<a id="service" style="margin:0 0 70px 0;display:block;color:white;">service</a>
	<?php $page = PageTable::doFetchOne('id, title, title_de, title_it, title_ko, content, content_de, content_it, content_ko, updated_at', array('type'=>'service'))?>
	<?php include_partial("main/content_culture", array('page'=>$page));?>
	
	<!--items-->
	<?php $rss = ItemTable::doExecute('id, title, title_de, title_it, title_ko, summary, summary_de, summary_it, summary_ko, content, content_de, content_it, content_ko, updated_at', 
																		array('pageId'=>$page->getId(), 'limit'=>4))?>
	
	<?php $k = 0; foreach($rss as $rs):?>
		<div class="left" style="margin:0 <?php echo ++$k%2 ? 29 : 0?>px 0 0;width:460px;height:280px;text-align:justify;"> 
			<a class="fancybox" href="#fancybox-service-<?php echo $rs->getId()?>">
					<h2 style="text-align:left;"><?php echo $rs?></h2>
					<?php echo utf8_substr(GlobalLib::clearOutput($rs->getSummaryCulture()), 0, 190)?> ..
			</a>
			<br clear="all">
			<br clear="all">
			
			<a href="#fancybox-service-<?php echo $rs->getId()?>" class="fancybox right showmore" style="margin:70px 0 0 0;">
					<?php echo __('Show more')?> <?php echo image_tag('icons/arrow-right.png', array('alt'=>__('Show more'), 'title'=>__('Show more'))) ?></a>

			<!-- featured images-->
			<?php $images = ImageTable::doFetchArray('filename', array('objectType'=>'item', 'objectId'=>$rs->getId(), 'isFeatured'=>1, 'orderBy'=>'sort ASC', 'limit'=>3));?>
			<?php $j = 0; foreach ($images as $image) {?>
					<?php echo image_tag('/u/images/t200-'.$image['filename'], array('class'=>'left border-radius-90', 'alt'=>$rs, 'title'=>$rs, 'style'=>'width:100px;height:95px;border:1px solid #f2f2f2;margin:0 0 0 -'.$j.'px;'));?>
					<?php $j = 20;?>
			<?php }?>
		</div>

		<!--fancy hidden content-->
		<div id="fancybox-service-<?php echo $rs->getId()?>" class="fancy-custom">
			<h2 style="text-align:left;"><?php echo $rs?></h2>
			<div class="timestamp"><?php echo __('Last updated');?> <?php echo time_ago($rs->getUpdatedAt())?></div>
			<?php if($tmp = $rs->getContentCulture()):?>
				<?php echo $tmp?>
				<br clear="all">
				<br clear="all">
				<br clear="all">
			<?php endif?>
			
			<!--main image-->
			<?php if($rs->getImage1()):?>
				<?php echo image_tag('/u/item/'.$rs->getImage1(), array('style'=>'max-width:950px;border-radius:5px;'));?>
				<br clear="all">
				<br clear="all">
			<?php endif?>
				
			<!--images-->
			<?php $images = ImageTable::doExecute('filename, title, title_de, title_it, title_ko, description, description_de, description_it, description_ko', 
                  													array('objectType'=>'item', 'objectId'=>$rs->getId(), 'orderBy'=>'sort ASC', 'limit'=>100));?>
			<?php foreach ($images as $image) {?>
				<b><?php echo GlobalLib::clearOutput($image);?></b>
				<br clear="all">
				<?php echo GlobalLib::clearOutput($image->getDescCulture());?>
				<br clear="all">
				<br clear="all">
				<?php echo image_tag('/u/images/'.$image->getFilename(), array('class'=>'image', 'alt'=>$image, 'title'=>$image, 'style'=>'width:920px;'));?>
				<br clear="all">
				<br clear="all">
				<br clear="all">
				<hr class="dotted">
				<br clear="all">
				<br clear="all">
			<?php }?>
		</div>
	<?php endforeach?>
</div>
<br clear="all">
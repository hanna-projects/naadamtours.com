<!--team-->

<div class="page">
	<a id="team" style="margin:0 0 70px 0;display:block;color:white;">team</a>

	<?php $page = PageTable::doFetchOne('id, title, title_de, title_it, title_ko, content, content_de, content_it, content_ko, updated_at', array('type'=>'team'))?>
	<?php include_partial("main/content_culture", array('page'=>$page));?>

	<a onclick="toggler('#togglediv-team', '#toggler-team');" id="toggler-team" class="right showmore" style="cursor:pointer;">
		<?php echo __('Show more')?> <?php echo image_tag('icons/arrow-down.png', array('alt'=>__('Show more'), 'title'=>__('Show more'))) ?>
	</a>
	<br clear="all">
	<script type="text/javascript">
		function toggler(div_id, a_id) {
			if ($(div_id).css('display') == "block") {
				$(a_id).html('<?php echo __('Show more')?> <?php echo image_tag('icons/arrow-down.png', array('alt'=>__('Show more'), 'title'=>__('Show more'))) ?>');
			} else {
				$(a_id).html('<?php echo __('Hide')?> <?php echo image_tag('icons/arrow-up.png', array('alt'=>__('Hide'), 'title'=>__('Hide'))) ?>');
			}
			$(div_id).toggle('slow', 'linear');
		}
	</script>
	
	<div id="togglediv-team" style="display:none;">
		<!-- items - team members -->
		<?php $rss = ItemTable::doExecute('id, route, title, title_de, title_it, title_ko, image1, summary, summary_de, summary_it, summary_ko, content, content_de, content_it, content_ko, updated_at', 
																			array('pageId'=>$page->getId(), 'limit'=>4))?>
		<?php $i = 0;?>
		<?php foreach($rss as $rs):?>
			<div class="left team-members" style="width:200px;height:370px;padding:10px 35px 0 0;margin:0 0 30px 0;text-align:justify;"> 
				<?php echo image_tag('/u/item/'.$rs->getImage1(), array('style'=>'max-width:200px;', 'alt'=>$rs, 'title'=>$rs)) ?>
				<h2 style=""><?php echo $rs?></h2>
				<div class="timestamp"><?php echo __('Last updated');?> <?php echo time_ago($page->getUpdatedAt())?></div>
				<?php echo GlobalLib::clearOutput($rs->getSummaryCulture())?>
				<br clear="all">
			</div>
		<?php endforeach?>
		<br clear="all">
		<br clear="all">

		<!-- membership and certificates -->
		<?php $page = PageTable::doFetchOne('id, title, title_de, title_it, title_ko, content, content_de, content_it, content_ko, updated_at', array('type'=>'certificate'))?>
		<h3 style="text-align:left;"><?php echo $page?></h3>
		<div class="timestamp"><?php echo __('Last updated');?> <?php echo time_ago($page->getUpdatedAt())?></div>
		<?php echo GlobalLib::clearOutput($page->getContentCulture())?>
		<br clear="all">
		<br clear="all">
		
		<!-- items -  certificates -->
		<?php $rss = ItemTable::doExecute('id, route, title, title_de, title_it, title_ko, image1, summary, summary_de, summary_it, summary_ko, content, content_de, content_it, content_ko, updated_at', 
																			array('pageId'=>$page->getId(), 'limit'=>4))?>
		<?php $i = 0;?>
		<?php foreach($rss as $rs):?>
			<?php echo image_tag('/u/item/'.$rs->getImage1(), array('class'=>'left', 'style'=>'margin:0 10px 10px 0', 'alt'=>$rs, 'title'=>$rs)) ?>
		<?php endforeach?>
		<br clear="all">
		<br clear="all">
	</div>
</div>

<!--page - yes-->
<!--items - yes-->
<!--subitems - no-->
<!--images - no-->
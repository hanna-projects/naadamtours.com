<?php $rss = GlobalTable::doExecute('Slide', 'id, filename, link, title, title_de, title_it, title_ko', array())?>
<div class="flexslider">
  <ul class="slides">
    <?php foreach ($rss as $rs):?>
        <li style="position:relative;">
		        <?php if($rs->getTitle()):?>
			        	<a href="<?php echo $rs->getLink() ? $rs->getLink() : '#'?>" target="_blank"  style="position:absolute;top:20%;right:2%;z-index:1;">
				        		<h2 style="color:white;font-weight:normal;font-style:italic;font-size:20px;">
				        				<?php echo __($rs)?>
				        		</h2>
			        	</a>
		        <?php endif?>
		      	<?php echo image_tag('/u/slide/'.$rs->getFilename(), array('style'=>'width:950px;height:400px;', 'class'=>'left', 'alt'=>$rs, 'title'=>$rs))?>
  			</li>
    <?php endforeach;?>
  </ul>
</div><!--flexslider-->

<br clear="all">
<br clear="all">
<br clear="all">

<script type="text/javascript">
$(window).load(function() {
	$('.flexslider').flexslider({
		slideshow: true,
		animation: "fade",
		animationLoop: true,
		directionNav: false,
		itemWidth:950,
		itemHeight:500,
		keyboard: true,
		touch: true,
		start: function(slider) {
			$('.slides li img').click(function(event){
				event.preventDefault();
				slider.flexAnimate(slider.getTarget('next'));
			});
		}
	});
});
</script>
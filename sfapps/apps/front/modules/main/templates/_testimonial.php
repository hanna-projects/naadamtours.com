<div id="testimonial" class="page left" style="width:450px;padding:15px 0;z-index:1;">

		<h3><?php echo __('Testimonials')?></h3>
		<?php $rss = GlobalTable::doExecute('Testimonial', 'id, text, text_de, text_it, text_ko, fullname, position, date, signature', array())?>
		<br clear="all">
		<br clear="all">
		<div class="flexslider-testimonial">
		  <ul class="slides">
		    <?php foreach ($rss as $rs):?>
		        <li style="text-align:center;">
		        		<div class="timestamp"><?php echo time_ago($rs->getDate())?></div>
		        		<div style="text-align:justify;"><?php echo GlobalLib::clearOutput($rs->getTextCulture())?></div>
						<br clear="all">
		        		<b><?php echo $rs->getFullname()?></b><br>
		        		<?php echo $rs->getPosition()?><br>
		      			<?php echo image_tag('/u/testimonial/'.$rs['signature'], array('class'=>'left', 'style'=>'max-width:120px;max-height:80px;margin:5px 0 0 37%;', 'alt'=>$rs->getFullname(), 'title'=>$rs->getFullname()))?>
		  			</li>
		    <?php endforeach;?>
		  </ul>
		</div><!--flexslider-->

		<br clear="all">		
		<br clear="all">
</div><!--testimonial-->


<script type="text/javascript">
$(window).load(function() {
  $('.flexslider-testimonial').flexslider({
    animation: "fade",
    directionNav: false,
    itemWidth:450,
    itemHeight:250,
  });
});
</script>

<!--table - testimonial-->
<!--page - no-->
<!--items - no-->
<!--subitems - no-->
<!--images - no-->
<?php if($rs->getCover()) echo image_tag('/u/offer/'.$rs->getCover(), array('class'=>'image', 'style'=>'max-width:949px', 'alt'=>$rs, 'title'=>$rs));?>

<div id="offer-header" style="background:white;width:950px;z-index:1;">
		<h1 style="border-color:#9781a1;"><?php echo $rs?></h1>
		<div class="left highlight">
				<?php echo $rs->getNbDays().' '.__('days')?> 
				<?php if($rs->getPriceFrom()) echo __('from').' '.$rs->getPriceFrom().' per person'?> 
				<?php if($rs->getGuests()) echo '&nbsp; | &nbsp;'.__('guests').': '.$rs->getGuests()?>
				<?php if($rs->getAges()) 	 echo '&nbsp; | &nbsp;'.__('age')		.': '.$rs->getAges()?>
		</div>
		
		<div class="right">
				<a href="<?php echo url_for('page/generatePdf?route='.$rs->getRoute())?>" class="highlight"><?php echo __("Save this page as PDF")?></a> &nbsp; | &nbsp;
				<a href="#offer-enquiry" class="highlight"><?php echo __("Enquire as a travel agent")?></a>
		</div>
		<br clear="all">
</div>
<br clear="all">

<script type="text/javascript">
$(document).ready(function(){ // sticky
    $("#offer-header").sticky({topSpacing:60});
});
</script>

<table width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<td width="65%">
				<?php echo GlobalLib::clearOutput($rs->getOverviewCulture())?>
		</td>
		<td width="5%">&nbsp;</td>
		<td width="30%">
				<?php if($rs->getMap()) echo image_tag('/u/offer/'.$rs->getMap(), array('class'=>'image', 'style'=>'width:450px;margin:5px 0;', 'alt'=>$rs, 'title'=>$rs));?>
		</td>
	</tr>
	<tr>
			<td colspan="3">
				<br clear="all">
				<h4 style="width:100px;float:left;margin:-4px 0 0 0;">Inclusions</h4>
				<?php echo image_tag('icons/offer-meals.png', array('align'=>'absmiddle', 'alt'=>__('Meals'), 'title'=>__('Meals')))?> <?php echo __('Meals')?>
				<!--<span style="color:#999;"><?php //echo GlobalLib::clearOutput($rs->getMeal())?></span>-->
				&nbsp;&nbsp;
				<?php echo image_tag('icons/offer-transport.png', array('align'=>'absmiddle', 'alt'=>__('Transport'), 'title'=>__('Transport')))?> <?php echo __('Transport')?>
				<!--<span style="color:#999;"><?php //echo GlobalLib::clearOutput($rs->getTransport())?></span>-->
				&nbsp;&nbsp;
				<?php echo image_tag('icons/offer-accomo.png', array('align'=>'absmiddle', 'alt'=>__('Accomodation'), 'title'=>__('Accomodation')))?> <?php echo __('Accomodation')?>
				<!--<span style="color:#999;"><?php //echo GlobalLib::clearOutput($rs->getAccommodation())?></span>-->
				&nbsp;&nbsp;
				<?php echo image_tag('icons/offer-activi.png', array('align'=>'absmiddle', 'alt'=>__('Included Activities'), 'title'=>__('Included Activities')))?> <?php echo __('Included Activities')?>
				<!--<span style="color:#999;"><?php //echo GlobalLib::clearOutput($rs->getIncludedActivities())?></span>-->
			</td>
	</tr>
	<tr>
		<td>
				<br clear="all">
				<?php if($tmp = $rs->getHighlightsCulture()): ?>
						<h4>Highlights</h4>
						<?php echo GlobalLib::clearOutput($tmp)?>
						<br clear="all">
						<br clear="all">
				<?php endif ?>
				<br clear="all">				
		</td>
		<td></td>
		<td>
				<br clear="all">
				<h4>Gallery</h4>
				<!--TODO - Title & Desc-->
				<?php $images = ImageTable::doExecute('filename, title, title_de, title_it, title_ko, description, description_de, description_it, description_ko', 
					array('objectType'=>'offer', 'objectId'=>$rs->getId(), 'orderBy'=>'sort ASC', 'limit'=>100));?>
				<div class="flexslider-offer">
					  <ul class="slides">
					    <?php foreach ($images as $image):?>
					        <li><a>
					      			<?php echo image_tag('/u/images/'.$image->getFilename(), array('style'=>'width:450px;height:280px;', 'alt'=>$image, 'title'=>$image))?>
					      			<!--<b><?php //echo GlobalLib::clearOutput($image);?></b><br>-->
									<?php //echo GlobalLib::clearOutput($image->getDescCulture());?>
					  			</a></li>
					    <?php endforeach;?>
					  </ul>
				</div><!--flexslider-->
				<br clear="all">
				<script type="text/javascript">
				$(window).load(function() {
					  $('.flexslider-offer').flexslider({
						    slideshow: true,
							animation: "fade",
							animationLoop: true,
							directionNav: false,
							itemWidth:450,
							itemHeight:280,
							keyboard: true,
							touch: true,
							start: function(slider) {
								$('.slides li img').click(function(event){
									event.preventDefault();
									slider.flexAnimate(slider.getTarget('next'));
								});
							}
					  });
				});
				</script>
		</td>
	</tr>
	<tr>
		<td>
			<h4>Itinerary details</h4>
			<div class="itinerary">
				<ul>
					<?php $itineraries = ItineraryTable::doExecute('day, title, title_de, title_it, title_ko, image, content, content_de, content_it, content_ko', array('offerId'=>$rs->getId()));?>
					<?php foreach ($itineraries as $itinerary):?>
						<li>
						 <?php echo __('Day').' '.$itinerary->getDay()?> - <?php echo GlobalLib::clearOutput($itinerary);?>
							<span class="right"><?php echo $itinerary->getMeals()?></span>
							<br clear="all">
							<div class="desc">
								<?php echo image_tag('/u/itinerary/'.$itinerary->getImage(), array('style'=>'width:160px;max-height:120px;', 'class'=>'image', 'alt'=>$itinerary, 'title'=>$itinerary))?>
								<?php echo GlobalLib::clearOutput($itinerary->getContentCulture());?>
								<br clear="all">
							</div>							
						</li>
					<?php endforeach;?>
				</ul>
				<br clear="all">
			</div>
			<br clear="all">

			<?php if($tmp = $rs->getImportantNotes()): ?>
					<br clear="all">
					<h4>Important notes</h4>
					<?php echo GlobalLib::clearOutput($tmp)?>
					<br clear="all">
					<br clear="all">
			<?php endif ?>
		
			<?php if($tmp = $rs->getYoutube()): ?>
					<br clear="all">
					<h4>Video</h4>
					<?php echo $tmp?>
					<br clear="all">
					<br clear="all">
			<?php endif ?>
			
			<br clear="all">
			<a id="offer-enquiry"></a>
			<h4 style="border-bottom:1px solid #9781a1;">Enquiry</h4>
			<br clear="all">
			<div  style="width:410px;">
					<?php include_partial("main/enquiry", array());?>
			</div>
		</td>
		<td></td>
		<td>
			<br clear="all">
			<h4>Travel Info</h4>
			<?php $page = PageTable::doFetchOne('content, content_de, content_it, content_ko', array('type'=>'travel-info'))?>
			<?php echo GlobalLib::clearOutput($page->getContentCulture())?>
			<br clear="all">
			<a class="fancybox right showmore" onclick="$('#offer-travel-info').toggle();" style="cursor:pointer;">
					<?php echo __('Show more')?> <?php echo image_tag('icons/arrow-down.png', array('alt'=>__('Show more'), 'title'=>__('Show more'))) ?>
			</a>
			<br clear="all">
			<div id="offer-travel-info" style="display:none;">
					<?php include_partial("page/items", array('pageId'=>$page->getId()));?>
			</div>
		</td>
	</tr>
</table>
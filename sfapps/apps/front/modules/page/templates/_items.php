<?php $rss = ItemTable::doExecute('id, title, title_de, title_it, title_ko, summary, summary_de, summary_it, summary_ko, content, content_de, content_it, content_ko, updated_at', 
																 	array('pageId'=>$pageId))?>
<?php $i = 0;?>
<?php foreach($rss as $rs):?>
		<?php $id = $rs->getId()?>
		<h5><?php echo $rs?></h5>
		<div class="timestamp"><?php echo __('Last updated');?> <?php echo time_ago($rs->getUpdatedAt())?></div>
		<?php if($rs->getImage1()) echo image_tag('/u/item/'.$rs->getImage1(), array('style'=>'margin:4px 10px 5px 0;', 'class'=>'left image', 'alt'=>$rs, 'title'=>$rs)) ?>
		<?php echo GlobalLib::clearOutput($rs->getContentCulture())?>	
		<br clear="all">
		<br clear="all">
		<br clear="all">
<?php endforeach?>
<?php include_partial("main/content_culture", array('page'=>$page));?>

<hr/>

<?php echo pager($pager, 'page/blog')?>
<br clear="all">

<!--items-->
<table width="100%">
	<?php $i = 0;?>
	<?php foreach($pager->getResults() as $rs):?>
		<?php $id = $rs->getId()?>				
		<tr>
			<td colspan="2">
				<a name="<?php echo $rs->getRoute()?>"></a>
				<br clear="all">
				<br clear="all">
				<br clear="all">
				<br clear="all">
			</td>
		</tr>
		<tr>
			<td>
				<h1 style="width:290px;line-height:50px;padding-bottom:10px;"><?php echo $rs?></h1>
				<div class="timestamp"><?php echo __('Last updated');?> <?php echo time_ago($rs->getUpdatedAt())?></div>
			</td>
			<td>
				<div style="width:650px;text-align:justify;line-height:30px;font-size:16px;">
					<?php echo GlobalLib::clearOutput($rs->getSummaryCulture())?>
					<br clear="all">
					<?php if($rs->getImage1()) echo image_tag('/u/item/'.$rs->getImage1(), array('style'=>'width:650px;margin:5px 0;', 'class'=>'left image', 'alt'=>$rs, 'title'=>$rs)) ?>
					<?php echo GlobalLib::clearOutput($rs->getContentCulture())?>	
					<br clear="all">
					<br clear="all">
			
					<!--images-->
					<?php $images = ImageTable::doExecute('filename, title, title_de, title_it, title_ko, description, description_de, description_it, description_ko', 
																								array('objectType'=>'item', 'objectId'=>$id, 'orderBy'=>'sort ASC', 'limit'=>100));?>
					<?php foreach ($images as $image) {?>
							<b><?php echo GlobalLib::clearOutput($image);?></b>
							<?php echo image_tag('/u/images/'.$image->getFilename(), array('class'=>'image', 'alt'=>$image, 'title'=>$image, 'style'=>'width:650px;margin:5px 0;'));?><br>
							<div style="text-align:justify;line-height:30px;font-size:16px;">
								<?php echo GlobalLib::clearOutput($image->getDescCulture());?>
							</div>
							<br clear="all">
					<?php }?>
					
					<?php echo image_tag('line.png', array('style'=>'margin:40px 0 0 35%'));?>
				</div>
			</td>
		</tr>
	<?php endforeach?>
</table>
		
<br clear="all">
<?php echo pager($pager, 'page/blog')?>
<br clear="all">

<!--page->items->images-->
<!--TODO: subitems?-->

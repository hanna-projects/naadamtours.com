<?php //if($rs->getCover()) echo image_tag('/u/dest/'.$rs->getCover(), array('class'=>'image', 'style'=>'max-width:949px', 'alt'=>$rs, 'title'=>$rs));?>
<?php $images = ImageTable::doExecute('filename, title, title_de, title_it, title_ko, description, description_de, description_it, description_ko', 
		  																			array('objectType'=>'dest', 'objectId'=>$rs->getId(), 'limit'=>100));?>
<div class="flexslider-dest">
	  <ul class="slides">
	    <?php foreach ($images as $image):?>
	        <li><a>
				<?php echo image_tag('/u/images/'.$image->getFilename(), array('class'=>'image', 'style'=>'width:950px;height:400px;', 'alt'=>$image, 'title'=>$image))?>
				<b><?php echo GlobalLib::clearOutput($image);?></b><br>
					<?php echo GlobalLib::clearOutput($image->getDescCulture());?>
			</a></li>
	    <?php endforeach;?>
	  </ul>
</div><!--flexslider-->
<br clear="all">

<script type="text/javascript">
$(window).load(function() {
	$('.flexslider-dest').flexslider({
		slideshow: true,
		animation: "fade",
		animationLoop: true,
		directionNav: false,
		itemWidth:950,
		itemHeight:500,
		keyboard: true,
		touch: true,
		start: function(slider) {
			$('.slides li img').click(function(event){
				event.preventDefault();
				slider.flexAnimate(slider.getTarget('next'));
			});
		}
	});
});
</script>

<h1 style="border-color:#9781a1;"><?php echo $rs?></h1>
<div class="left" style="width:560px;">
	<?php //if($rs->getMap()) echo image_tag('/u/dest/'.$rs->getMap(), array('class'=>'image', 'style'=>'float:right;max-width:450px;margin:2px 0 5px 5px;', 'alt'=>$rs, 'title'=>$rs));?>
	<?php echo GlobalLib::clearOutput($rs->getOverviewCulture())?>
	<br clear="all">
	<br clear="all">
	
	<div style="width:410px;margin:20px 0 0 0;">
		<h4 style="border-bottom:1px solid #9781a1;">Enquiry</h4>
		<br clear="all">
		<?php include_partial("main/enquiry", array());?>
	</div>
</div>

<div class="right" style="width:370px;">
	<?php $offers = OfferTable::doExecute('id, route, title, title_de, title_it, title_ko, thumb, intro, intro_de, intro_it, intro_ko, nb_days, price_from, guests', 
							array('ids'=>explode(";", $rs->getOfferIds())))?>
	<?php $i = 0;?>
	<?php foreach($offers as $offer):?>
		<div style="width:370px;height:280px;text-align:left;"> 
			<a href="<?php echo url_for('page/offer?route='.$offer->getRoute())?>">
				<?php echo image_tag('/u/offer/'.$offer->getThumb(), array('style'=>'width:370px;height:150px;margin-bottom:7px;', 'class'=>'image', 'alt'=>$offer, 'title'=>$offer)) ?>
				<h2 style="text-align:left;width:370px;"><?php echo $offer?></h2>
				<?php echo GlobalLib::clearOutput($offer->getIntroCulture())?>
				<div class="timestamp" style="font-size:12px;">
						<?php echo $offer->getNbDays().' '.__('days')?> 
						<?php if($offer->getPriceFrom()) echo __('from').' '.$offer->getPriceFrom().' per person'?>
				</div>
			</a>
			<a href="<?php echo url_for('page/offer?route='.$offer->getRoute())?>" class="right" style="margin:-23px 0 0 0;">
					<?php echo image_tag('icons/showmore.png', array('alt'=>__('Show more'), 'title'=>__('Show more'))) ?>
			</a>
			<br clear="all">
		</div>
		<br clear="all">
		<br clear="all">
	<?php endforeach?>	
</div>
<br clear="all">
<br clear="all">
<br clear="all">

<?php $page = PageTable::doFetchOne('content, content_de, content_it, content_ko', array('type'=>'travel-info'))?>
<h3 style="text-align:left;"><?php echo $page?></h3>
<?php echo GlobalLib::clearOutput($page->getContentCulture())?>
<br clear="all">
<br clear="all">
<?php include_partial("page/items", array('pageId'=>$page->getId()));?>
<br clear="all">


<script>
  $(function() {
    $( "#tabs" ).tabs();
  });
</script>
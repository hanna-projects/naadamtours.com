<?php

/**
 * page actions.
 *
 * @package    
 * @subpackage page
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 3335 2007-01-23 16:19:56Z fabien $
 */
class pageActions extends sfActions
{

    public function preExecute()
    {
    }

    public function executeShow(sfWebRequest $request)
    {
        $this->page = $page = Doctrine::getTable('Page')->findOneBy('route', $request->getParameter('route'));
		$this->forward404Unless($page);
    }
    
    public function executeOffer(sfWebRequest $request)
    {
        $this->rs = $rs = Doctrine::getTable('Offer')->findOneBy('route', $request->getParameter('route'));
        $this->forward404Unless($rs);
    }
    
    public function executeDest(sfWebRequest $request)
    {
        $this->rs = $rs = Doctrine::getTable('Destination')->findOneBy('route', $request->getParameter('route'));
        $this->forward404Unless($rs);
    }
    
    public function executeBlog(sfWebRequest $request)
    {
        $this->page = $page = PageTable::doFetchOne('id, title, title_de, title_it, title_ko, content, content_de, content_it, content_ko, updated_at', array('type'=>'media'));
        $this->forward404Unless($page);
        
        $this->pager = GlobalTable::getPager('Item', 'id, title, title_de, title_it, title_ko, summary, summary_de, summary_it, summary_ko, content, content_de, content_it, content_ko, updated_at', array('pageId'=>$page->getId()));
    }
    
    public function executeGeneratePdf(sfWebRequest $request) 
		{
				$route = $request->getParameter('route');
				//$rs = Doctrine::getTable('Offer')->findOneBy('route', $route);
        //$this->forward404Unless($rs);
        //$this->getContext()->getResponse()->clearHttpHeaders();
				try {
				    $client = new Pdfcrowd("dulamkhand", "037eaa9fd268bd0c76883d94b86809d9");
				    $path = sfConfig::get('app_host').'index.php/page/offer/route/'.$route;
				    $pdf = $client->convertURI($path);
				    header("Content-Type: application/pdf");
				    header("Cache-Control: max-age=0");
				    header("Accept-Ranges: none");
				    //header("Content-Disposition: attachment; filename=\"{$route}\"");
				    header("Content-Disposition: attachment; filename=\"google_com\"");
				    echo $pdf;
				} catch(PdfcrowdException $why) {
				    echo "Pdfcrowd Error: " . $why;
				}
				exit;
		}

}

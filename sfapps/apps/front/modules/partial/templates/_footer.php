<br clear="all">
<div style="padding:10px 0;border-top:1px solid #666;margin:50px 0 0 0;">
	<div class="left" style="width:550px;">
		<?php echo __('Ena Travel Mongolia are pioneers of developing responsible tourism in Mongolia and are dedicated to a healty Mongolian environment and the traditional culture! Our')?>
	</div>
	<ul class="right align-left">
        <li style="margin:10px 20px 0 0;"><a href="https://facebook.com" target="_blank"><?php echo image_tag('icons/social/20_fb.png', array('alt'=>'Facebook', 'title'=>'Facebook'))?></a></li>
        <li style="margin:10px 20px 0 0;"><a href="https://twitter.com" target="_blank"><?php echo image_tag('icons/social/20_tw.png', array('alt'=>'Twitter', 'title'=>'Twitter'))?></a></li>
        <li style="margin:10px 20px 0 0;"><a href="https://instagram.com" target="_blank"><?php echo image_tag('icons/social/20_ins.png', array('alt'=>'Instagram', 'title'=>'Instagram'))?></a></li>
        <li style="margin:10px 20px 0 0;"><a href="https://pinterest.com" target="_blank"><?php echo image_tag('icons/social/20_pin.png', array('alt'=>'Pinterest', 'title'=>'Pinterest'))?></a></li>
        <li style="margin:10px 20px 0 0;"><a href="https://blogger.com" target="_blank"><?php echo image_tag('icons/social/20_blog_gray.png', array('alt'=>'Blogger', 'title'=>'Blogger'))?></a></li>
  </ul>
	<br clear="all">
</div>
<div style="padding:10px 0 5px 0;border-top:1px solid #74677a;">
		<ul class="left align-left">
        <li class="left"><a href="<?php echo url_for('page/show?route=safety-and-privacy')?>"><?php echo __('Safety and Privacy')?></a></li>
        <li style="margin:0 0 0 15px;">|</a></li>
        <li style="margin:0 0 0 20px;"><a href="<?php echo url_for('page/show?route=terms-and-conditions')?>"><?php echo __('Terms and Conditions')?></a></li>
        <li style="margin:0 0 0 15px;">|</a></li>
        <li style="margin:0 0 0 20px;"><a href="<?php echo url_for('@homepage')?>#contact"><?php echo __('Contact Us')?></a></li>
  	</ul>
  	<ul class="right align-left">
        <li class="left">+976-9902-2507</li>
        <li style="margin:0 0 0 15px;">|</a></li>
        <li style="margin:0 0 0 15px;"><a href="mailto:<?php echo sfConfig::get('app_contact_email')?>"><?php echo sfConfig::get('app_contact_email')?></a></li>
  	</ul>
  	<br clear="all">
</div>

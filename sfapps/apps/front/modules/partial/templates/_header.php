<div id="header">
    <div class="left">
		<a href="<?php echo url_for('main/culture?l=en')?>"><?php echo image_tag('icons/flag-en.png', array('class'=>'left', 'style'=>'margin:0 2px 0 0', 'alt'=>'English', 'title'=>'English'))?></a>
		<!--<a href="<?php echo url_for('main/culture?l=de')?>"><?php //echo image_tag('icons/flag-de.png', array('class'=>'left', 'style'=>'margin:0 2px 0 0', 'alt'=>'Deutsche', 'title'=>'Deutsche'))?></a>-->
		<a href="<?php echo url_for('main/culture?l=it')?>"><?php echo image_tag('icons/flag-it.png', array('class'=>'left', 'style'=>'margin:0 2px 0 0', 'alt'=>'Italiano', 'title'=>'Italiano'))?></a>
		<!--<a href="<?php echo url_for('main/culture?l=ko')?>"><?php //echo image_tag('icons/flag-ko.png', array('class'=>'left', 'style'=>'margin:0 2px 0 0', 'alt'=>'한국어', 'title'=>'한국어'))?></a>-->
    </div>
	  
    <div class="right" style="width:184px;">
        <table>
            <tr>
                <!--<td>E-mail</td>
                <td>|</td>-->
                <td><a href="mailto:<?php echo sfConfig::get('app_contact_email')?>"><?php echo sfConfig::get('app_contact_email')?></a></td>
                <!--<td>|</td>-->
            </tr>
            <tr>
                <!--<td>Phone</td>
                <td>|</td>-->
                <td>+976-9902-2507</td>
                <!--<td>|</td>-->
            </tr>
            <tr>
                <td colspan="4">
                    <input value="<?php echo $sf_params->get('s') ? $sf_params->get('s') : __('Search')?>" 
                        name="search" id="search" style=""/>
                </td>
            </tr>
        </table>
    </div>
    
    <!--logo-->
	  <a href="<?php echo url_for('@homepage')?>" style="margin:0 auto;width:400px;display:block;">
        <?php echo image_tag('logo400.jpg', array('style'=>'max-width:;', 'alt'=>sfConfig::get('app_webtitle'), 'title'=>sfConfig::get('app_webtitle')))?>
    </a>
    
    <br clear="all">
		<br clear="all">		
		
    <!--menu-->
    <hr class="line"/>
    <?php $mod = isset($mod) ? $mod : $sf_request->getParameter('module'); ?>
    <?php $act = isset($act) ? $act : $sf_request->getParameter('action'); ?>
    <ul id="mainmenu">
        <li><a href="<?php if($act!='home') echo url_for('@homepage')?>#service" style="margin-left:0;">
        		<?php echo __('Service')?>
    		</a></li>
        <li><a href="<?php if($act!='home') echo url_for('@homepage')?>#offer">
        		<?php echo __('Tours')?>
        </a></li>
        <li><a href="<?php if($act!='home') echo url_for('@homepage')?>#destination">
        		<?php echo __('Destinations')?>
        </a></li>
        <li><a href="<?php if($act!='home') echo url_for('@homepage')?>#media">
        		<?php echo __('Blog')?>
        </a></li>
        <li><a href="<?php if($act!='home') echo url_for('@homepage')?>#team">
        		<?php echo __('About Us')?>
        </a></li>
        <li><a href="<?php if($act!='home') echo url_for('@homepage')?>#contact">
        		<?php echo __('Contact Us')?>
        </a></li>
        <li style="float:right;width:70px;margin:0;position:relative;">
		        <a href="https://facebook.com" target="_blank" style="margin:0;position:absolute;top:-10px;left:-10px;z-index:1;"><?php echo image_tag('icons/social/20_fb.png', array('alt'=>'Facebook', 'title'=>'Facebook'))?></a>
		        <a href="https://twitter.com" target="_blank" style="margin:0;position:absolute;top:-10px;left:20px;z-index:1;"><?php echo image_tag('icons/social/20_tw.png', array('alt'=>'Twitter', 'title'=>'Twitter'))?></a>
		        <a href="https://plus.google.com/" target="_blank" style="margin:0;position:absolute;top:-10px;left:50px;z-index:1;"><?php echo image_tag('icons/social/20_g.png', array('alt'=>'Google Plus', 'title'=>'Google Plus'))?></a>
		        <a href="https://instagram.com" target="_blank" style="margin:0;position:absolute;top:15px;left:-10px;z-index:1;"><?php echo image_tag('icons/social/20_ins.png', array('alt'=>'Instagram', 'title'=>'Instagram'))?></a>
		        <a href="https://pinterest.com" target="_blank" style="margin:0;position:absolute;top:15px;left:20px;z-index:1;"><?php echo image_tag('icons/social/20_pin.png', array('alt'=>'Pinterest', 'title'=>'Pinterest'))?></a>
		        <a href="https://blogger.com" target="_blank" style="margin:0;position:absolute;top:15px;left:50px;z-index:1;"><?php echo image_tag('icons/social/20_blog_gray.png', array('alt'=>'Blogger', 'title'=>'Blogger'))?></a>
        </li>
		<br clear="all">
				<!--<div style="position:relative;z-index:1000;height:2px;background:#fff;margin-top:-2px;"></div>-->
    </ul>
</div>

<script type="text/javascript">
/* menu sticky */
$(document).ready(function(){
    $("#mainmenu").sticky({topSpacing:-2});
});

/* search */
$('#search').click(function(){
    if($(this).val().trim() == "<?php echo __('Search')?>") { $(this).val(''); }
}).blur(function() {
    if($(this).val().trim() == "") { $(this).val("<?php echo __('Search')?>"); }
});

/* showmore/hide toggler */
function toggler(div_id, a_id) {
	if ($(div_id).css('display') == "block") {
		$(a_id).html('<?php echo __('Show more')?> <?php echo image_tag('icons/arrow-down.png', array('alt'=>__('Show more'), 'title'=>__('Show more'))) ?>');
	} else {
		$(a_id).html('<?php echo __('Hide')?> <?php echo image_tag('icons/arrow-up.png', array('alt'=>__('Hide'), 'title'=>__('Hide'))) ?>');
	}
	$(div_id).toggle('slow', 'linear');
}
</script>

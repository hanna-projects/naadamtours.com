<?php $host = sfConfig::get('app_host')?>
<head>
	<?php include_http_metas() ?>
	<?php include_metas() ?>
	<?php include_title() ?>
	<link rel="shortcut icon" href="<?php echo $host?>images/vectors/k29973617b.jpg" />

	<!-- jquery -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E="
  		crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
  		crossorigin="anonymous"></script>

	<!-- <script src="<?php //echo $host?>js/jquery.min.js"></script> -->
	<!-- <script src="<?php //echo $host?>addons/ui/jquery-ui.min.js"></script> -->

	<!-- bootstrap -->
	<link rel="stylesheet" type="text/css" media="screen" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"/>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>

	<!-- addons -->
	<?php use_stylesheet('/addons/ui/jquery-ui.css') ?>

    <?php use_javascript('/addons/sticky/jquery.sticky.js');?>    
    <?php use_javascript('/addons/scrollup/jquery.scrollUp.min.js');?>
    
    <?php use_javascript('/addons/flexslider/jquery.flexslider-min.js');?>    
    <?php use_stylesheet('/addons/flexslider/flexslider.css');?>
    <?php use_stylesheet('/addons/flexslider/flexslider-testimonial.css');?>
    <?php use_stylesheet('/addons/flexslider/flexslider-offer.css');?>
    <?php use_stylesheet('/addons/flexslider/flexslider-dest.css');?>
	
	<?php use_javascript('/addons/fancybox/jquery.fancybox.pack.js');?>
	<?php use_javascript('/addons/fancybox/jquery.mousewheel-3.0.6.pack.js');?>
    <?php use_stylesheet('/addons/fancybox/jquery.fancybox.css');?>
    
    <?php use_stylesheet('/addons/fonts/open-sans.css') ?>
    <?php use_stylesheet('/addons/fonts/roboto.css') ?>  

	<?php include_stylesheets() ?>
	<?php include_javascripts() ?>

	<script type="text/javascript">
		$(function(){
			$.scrollUp({scrollText:''});
		});
	</script>
</head>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<?php include_partial("partial/head", array());?>
<body>
	<!--<div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&appId=623636837674066&version=v2.0";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>-->

	<div class="wrapper">
        <?php include_partial("partial/header", array());?>
				<?php //include_partial("partial/bannerTop", array());?>
		  
				<!--flash message-->
				<?php if ($sf_user->hasFlash('flash')): ?>
					<div class="flash"><?php echo $sf_user->getFlash('flash')?></div>
				<?php endif; ?>
		
				<?php echo $sf_content ?>
		    
        <?php include_partial("partial/footer", array());?>  
    </div>
</body>
</html>
